def compute_word_occurences(string):
	splitted = string.split()
	keys = dict((key, {}) for key in set(splitted))
	
	for key in keys:
		indices = get_indices_in_list(str(key), splitted)
		for i in indices:
			if i < len(splitted) - 1:
				nextWord = splitted[i+1]
				
				if nextWord in keys[key]:
					keys[key][nextWord] += 1
				else:
					keys[key][nextWord] = 1
		
	
	return keys
	
def get_indices_in_list(string, list):
	result = []
	
	for i in range(0, len(list)):
		if string == list[i]:
			result.append(i)
	
	return result

from random import randint
	
def make_random_text(dict, numberOfWords):
	words = list(dict.keys())
	index = randint(0,len(dict)-1)
	
	lastWord = words[index]
	
	result = lastWord + " " 
	counter = 1
	
	while counter < numberOfWords:
		print(counter)
		print(result)
	
		follows = list(dict[lastWord].keys())
		
		if len(follows) == 0:
			continue
		
		index = randint(0, len(follows)-1)
		lastWord = follows[index]
		result += lastWord + " " 
		counter += 1
		
	return result
	
	
with open('faust_1_less', encoding='iso8859') as f:
    s = f.read()
	
dict = compute_word_occurences(s)
#print(dict)

text = make_random_text(dict,45)

print(text.split())
print(len(text.split()))