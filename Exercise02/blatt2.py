import py


# ____________________________________________________________
#
# AUFGABE 1
#
def compute_word_occurences(string):
	splitted = string.split()
	keys = dict((key, {}) for key in set(splitted))
	
	for key in keys:
		indices = get_indices_in_list(str(key), splitted)
		for i in indices:
			if i < len(splitted) - 1:
				nextWord = splitted[i+1]
				
				if nextWord in keys[key]:
					keys[key][nextWord] += 1
				else:
					keys[key][nextWord] = 1
		
	
	return keys
	
def get_indices_in_list(string, list):
	result = []
	
	for i in range(0, len(list)):
		if string == list[i]:
			result.append(i)
	
	return result

def test_word_occurences():
    d = compute_word_occurences("a b a b a c a c")
    assert d == {
        "a": {"b": 2, "c": 2},
        "b": {"a": 2},
        "c": {"a": 1}
    }

def test_word_occurences_long():
    with open('faust_1', encoding='iso8859') as f:
        s = f.read()
    #import pdb; pdb.set_trace()
    d = compute_word_occurences(s)
    assert d["FAUST:"]["Habe"] == 1
    assert d["Habe"]["nun,"] == 1
    assert d["nun,"]["ach!"] == 1
    assert d["GRETCHEN:"] == {
        'Wieso?': 1,
        'Kein': 1,
        'Nachbarin!': 1,
        'Mein': 2,
        'Meine': 1,
        'Weh!': 1,
        'Allm\xe4chtiger!': 1,
        'W\xe4r': 1,
        '(nach': 1,
        'Das': 2,
        'Mir': 1,
        'Ach!': 1,
        'Er': 1}

from random import randint		
		
def make_random_text(dict, numberOfWords):
	words = list(dict.keys())
	index = randint(0,len(dict)-1)
	
	lastWord = words[index]
	
	result = lastWord + " " 
	counter = 1
	
	while counter < numberOfWords:
		follows = list(dict[lastWord].keys())
		
		if len(follows) == 0:
			continue
		
		index = randint(0, len(follows)-1)
		lastWord = follows[index]
		result += lastWord + " " 
		counter += 1
		
	return result
		
def test_random_words():
    with open('faust_1', encoding='iso8859') as f:
        s = f.read()
    d1 = compute_word_occurences(s)
    text = make_random_text(d1, 100)
    d2 = compute_word_occurences(text)
    last = None
    assert len(text.split()) == 100
    for word in text.split():
        if last is not None:
            assert word in d1[last]
            assert word in d2[last]
        last = word
    print(text)


# ____________________________________________________________
#
# AUFGABE 2
#

class OpenClass(type):
	def __init__(self, name, bases, attrs):
		if name == "__enhance__":
			for key in attrs:				
				setattr(bases[0], key, attrs[key])
		else:
			return type.__init__(self, name, bases, attrs)
			#pass # muss unbedingt beachtet werden!

def test_open_class():
    class A(object, metaclass=OpenClass):

        def __init__(self, x, y):
            self.x = x
            self.y = y

        def f(self):
            return self.x + self.y

        value = 7

    a = A(2, 4)
    assert a.f() == 6
    assert a.value == 7
    py.test.raises(AttributeError, "a.g()")

    class __enhance__(A):
        value = 9

        def g(self):
            return self.x * self.y

    assert a.value == 9
    assert a.g() == 8



# ____________________________________________________________
#
# AUFGABE 3
#

overrides = dict() 

def test_mygetattr_override():
	class A(object):
		pass
	register_override(object, "fortytwo", 42)
	register_override(A, "fortyone", 41)
	print(overrides)
    a = A()
    assert mygetattr(a, "fortytwo") == 42
    assert mygetattr(a, "fortyone") == 41
    assert mygetattr([], "fortytwo") == 42
    py.test.raises(AttributeError, mygetattr, 1, "fortyone")


def register_override(obj, attr, value):
	if obj in overrides:
		overrides[obj][attr] = value
	else:
		overrides[obj] = { attr: value } 
	
def mygetattr(obj, name):
	klass = obj.__class__

	for cls in klass.mro():
		if cls in overrides:
			return overrides[cls][name]
		
		try:
			x = cls.__dict__[name]
			try:
				return x.__get__(obj, cls)
			except AttributeError:
				return x
		except KeyError:
			pass
	for cls in klass.mro():
		try:
			return cls.__dict__['__getattr__'](obj, name)
		except KeyError:
			pass
	raise AttributeError
