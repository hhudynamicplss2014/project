class OpenClass(type):
	def __init__(self, name, bases, attrs):
		if name == "__enhance__":
			for key in attrs:				
				setattr(bases[0], key, attrs[key])
				

			
class A(object, metaclass=OpenClass):
	def __init__(self, x, y):
		self.x = x
		self.y = y

	def f(self):
		return self.x + self.y

	value = 7

a = A(2,4)
print(a.f())
print(a.value)	
	
class __enhance__(A):
	value = 9

	def g(self):
		return self.x * self.y			
				
print(a.value)
print(a.g())