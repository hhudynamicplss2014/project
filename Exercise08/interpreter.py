import copy
import py
from simpleast import *

from simpleparser import parse
from objmodel import *

#
# The Interpreter class needs a method 'eval(ast, w_context)' that is
# doing "AST visiting", as follows:
#
#     def eval(self, ast, w_context):
#         method = getattr(self, "eval_" + ast.__class__.__name__)
#         return method(ast, w_context)
#
# This calls a method called "eval_Xxx" on self for every class
# called Xxx of the simpleast.py module.
#

class Interpreter(object):
    def eval(self, ast, w_context):
        method = getattr(self, "eval_" + ast.__class__.__name__)
        return method(ast, w_context)

    def eval_MetaNode(self, ast, w_context):
        pass

    def eval_AstNode(self, ast, w_context):
        pass

    def eval_Expression(self, ast, w_context):
        pass

    def eval_IntLiteral(self, ast, w_context):
        return W_Integer(int(ast.value))

    def eval_MethodCall(self, ast, w_context):
        receiver = self.eval(ast.receiver, w_context)

        print "Ast " + str(ast)
        print "Methodname " + str(ast.methodname)
        print "receiver dump"
        receiver.dump()

        method_object = copy.deepcopy(receiver.getvalue(ast.methodname))

        print "receiver " + str(receiver)
        print "method " + str(method_object)

        if isinstance(method_object, W_Method):
            print "This is a function"
            #method_ast ist der ast nicht nur der block!
            method_ast = method_object.reference # nicht ethod.getvalue("block")!!! #TODO: FunctionDefinition anpassen
            print method_ast
            for i in range(len(ast.arguments)):
                arg = self.eval(ast.arguments[i], method_object)
                print "the " + str(i) + " argument = " + str(arg) + " and is called " + str()
                method_object.setvalue(method_object.getvalue("arguments")[i], arg)

            print "mtehod"
            method_object.dump()

            print "fdkj"
            w_context.dump()

            return self.eval(method_ast.block, method_object)
        return method_object

    def eval_PrimitiveMethodCall(self, ast, w_context):
        pass

    def eval_ImplicitSelf(self, ast, w_context):
        return w_context

    def eval_Statement(self, ast, w_context):
        pass

    def eval_Assignment(self, ast, w_context):
        lvalue = self.eval(ast.lvalue, w_context)
        attrname = ast.attrname
        expression = self.eval(ast.expression, w_context)

        lvalue.setvalue(attrname, expression)
        return lvalue

    def eval_ExprStatement(self, ast, w_context):
        return self.eval(ast.expression, w_context)

    def eval_IfStatement(self, ast, w_context):
        condition = self.eval(ast.condition, w_context)

        print condition

        if condition.istrue():
            return self.eval(ast.ifblock, w_context)
        else:
            if ast.elseblock is not None:
                return self.eval(ast.elseblock, w_context)
            else:
                return w_context

    def eval_WhileStatement(self, ast, w_context):
        pass

    def eval_FunctionDefinition(self, ast, w_context):
        method = W_Method(ast)
        method.setvalue("name", ast.name)
        method.setvalue("arguments", ast.arguments)

        w_context.setvalue(ast.name, method)

        return w_context

    def eval_ObjectDefinition(self, ast, w_context):
        temp = self.eval(ast.block, W_NormalObject())
        w_context.setvalue(ast.name, temp)

        return w_context

    def eval_Program(self, ast, w_context):
        print ast

        for statement in ast.statements:
            w_context = self.eval(statement, w_context)

        return w_context


ast1 = parse("""
def f(x, y):
    if x:
        x
    else:
        y
i = f(6, 3)
j = f(0, 9)
""")

ast2 = parse("""
k = 10
if k:
    j = 11
else:
    i = 12
""")

ast0 = parse("""
k = 10
j = 11
i = 12
""")

ast3 = parse("""
object x:
    i = 4
    j = i
    object z:
        i = 5
""")

ast = parse("""
object a:
    def f(r):
        if r:
            x = 5
            r f(0)
        else:
            x = 7
        x
i = a f(4)
""")
w_module = W_NormalObject()

interpreter = Interpreter()
interpreter.eval(ast, w_module)
#print("fds")
#print(w_module)
#    assert w_module.getvalue("i").value == 6
#    assert w_module.getvalue("j").value == 9

w_module.dump()
