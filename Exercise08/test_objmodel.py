from objmodel import W_Integer, W_NormalObject


def test_normal_init():
    w1 = W_NormalObject()
    assert w1.getvalue('whatever') is None


def test_normal_setvalue():
    w1 = W_NormalObject()
    assert w1.getvalue('whatever') is None

    w1.setvalue('whatever', W_Integer(20))
    assert w1.getvalue('whatever') is not None
    assert w1.getvalue('whatever').value == 20

    w1.setvalue('whatever', False)
    assert w1.getvalue('whatever') == False

    w2 = W_NormalObject()
    assert w2.getvalue('whatever') is None

    w2.setvalue('whatever', 'string')
    assert w2.getvalue('whatever') is not None
    assert w2.getvalue('whatever') == 'string'


def test_normal_getvalue():
    w1 = W_NormalObject({'whatever': 'something'})
    assert w1.getvalue('whatever') == 'something'

    w1.setvalue('whatever', 'anything')
    assert w1.getvalue('whatever') == 'anything'


def test_normal_istrue():
    w1 = W_NormalObject({'something': 'anything'})
    assert w1.istrue() == True

    w2 = W_NormalObject()
    assert w2.istrue() == True


def test_normal_clone():
    w1 = W_NormalObject()
    w1.setvalue('abc', W_Integer(6))
    w2 = w1.clone()
    assert w2.getvalue('abc').value == 6
    w2.setvalue('abc', W_Integer(99))
    assert w2.getvalue('abc').value == 99
    assert w1.getvalue('abc').value == 6


def test_integer_init():
    w1 = W_Integer(5)
    assert w1.value == 5


def test_integer_setvalue():
    w1 = W_Integer(0)
    assert w1.value == 0

    w1.setvalue('value', 20)
    assert w1.value == 20

    w1.setvalue('something', 50)
    assert w1.value == 50

    w1.value = 0
    assert w1.value == 0


def test_integer_getvalue():
    w1 = W_Integer(0)
    assert w1.getvalue('value') is None
    assert w1.getvalue('something') is None


def test_integer_istrue():
    w1 = W_Integer(0)
    w2 = W_Integer(1)
    w3 = W_Integer(-1)

    assert w1.istrue() == False
    assert w2.istrue() == True
    assert w3.istrue() == True


def test_integer_clone():
    w1 = W_Integer(0)
    w2 = w1.clone()

    assert w1.value == 0
    assert w2.value == 0

    w1.value = 5
    w2.value = 10

    assert w1.value == 5
    assert w2.value == 10


def test_istrue():
    assert W_Integer(5).istrue() is True
    assert W_Integer(0).istrue() is False
    assert W_Integer(-1).istrue() is True
    # for now, only W_Integer(0) is false; all the other objects are true.
    assert W_NormalObject().istrue() is True

