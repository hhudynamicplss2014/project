
class W_Integer(object):
    def __init__(self, value):
        self.value = value

    def setvalue(self, key, value):
        self.value = value

    def getvalue(self, key):
        return None

    def istrue(self):
        return self.value != 0

    def clone(self):
        return W_Integer(self.value)

    def dump(self):
        print self.value


class W_NormalObject(object):
    def __init__(self, values = None):
        if values is None:
            values = {}

        self.values = values

    def setvalue(self, key, value):
        self.values[key] = value

    def getvalue(self, key):
        if key in self.values:
            return self.values[key]

        return None

    def istrue(self):
        return True

    def clone(self):
        return W_NormalObject(self.values.copy())

    def dump(self):
        for key in self.values:
            print(str(key) + " : " + str(self.values[key]))


# simulate W_Method by providing W_NormalObject having a mandatory value called ast_reference?

class W_Method(W_NormalObject):
    def __init__(self, reference):
        self.values = {}
        self.reference = reference

    def setreference(self, reference):
        self.reference = reference

    def getreference(self):
        return self.reference