import math

# Aufgabe 1

class Klass(object):
    def __init__(self, name, *parents):
        self.name = name
        self.parents = parents

    def getparents(self):
        return self.parents

    def merge(self, seqs):
        res = []
        i = 0
        while True:
            non_empty_sets = [seq for seq in seqs if seq]
            if not non_empty_sets:
                return res

            i += 1
            for seq in non_empty_sets:
                cand = seq[0]
                not_head = [s for s in non_empty_sets if cand in s[1:]]

                if not_head:
                    cand = None
                else:
                    break

            if not cand:
                raise "Inconstistent hierarchy"

            res.append(cand)

            for seq in non_empty_sets:
                if seq[0] == cand:
                    del seq[0]

    def get_mro(self):
        return self.merge([[self.__class__]] + map(self.get_mro(), self.__class__.__bases__)) + [list(self.__class__.__bases__)]


k1 = Klass("a")
k2 = Klass("b", k1)
k3 = Klass("c", k2, k1)
k4 = Klass("d")
k5 = Klass("e", k2,k4,k1)

def test_aufg1_simple():
    k1 = Klass("a")
    assert k1.get_mro() == [k1]
    k2 = Klass("b", k1)
    assert k2.get_mro() == [k2, k1]
    k3 = Klass("c", k2, k1)
    assert k3.get_mro() == [k3, k2, k1]
    k4 = Klass("d")
    assert k4.get_mro() == []
    k5 = Klass("e", k2,k4,k1)
    assert k5.get_mro() == [k2,k1,k4]

# Aufagbe 2
SQUARE = set([(0,0), (0,1),
              (1,0), (1,1)])

def test_aufg2_from_life_string():
    assert from_lifestring("") == set()
    assert from_lifestring("X") == set([(0,0)])
    assert from_lifestring("X X") == set([(0, 0), (2, 0)])

    assert from_lifestring("XX\nXX") == SQUARE
    assert from_lifestring("XX\nX ") == set([(0,0), (0,1), (1,0)])
   
def from_lifestring(string):
    result = set()

    if len(string) == 0:
        return result

    rows = string.split("\n")
    for i in range(len(rows)):
        row = rows[i]
        index = row.find("X")

        while index >= 0:
            result.add((index,i))
            index = row.find("X", index+1)

    return result

# Aufgabe 3

def test_aufg3_factorize():
    assert factorize(0) == set()
    assert factorize(-1) == set()
    assert factorize(1) == set([1])
    assert factorize(5) == set([1, 5])
    assert factorize(14) == set([1, 2, 7, 14])
    assert factorize(18) == set([1, 2, 3, 6, 9, 18])
    assert factorize(259) == set([1, 7, 37, 259])

def factorize(num):
	"""
	Find all factors of num and return them in a list.
	"""
	if num <= 0:
		return set()
	
	l = set()
	if num == 1:
		return set([1])
	for i in range(1, int(math.sqrt(num))):
		if num % i == 0:
			l.add(i)
			l.add(num // i)
	return l


def test_aufg3_hashset():
    l = []
    for i in range(20):
        assert not hashcontains(l, i)

    hashinsert(l,10)
    assert hashcontains(l,10)

    l = [None] * 5
    hashinsert(l, 3456)
    assert hashcontains(l, 3456)
    assert not hashcontains(l, 456)

    hashinsert(l, 456)
    assert hashcontains(l, 456)

def test_aufg3_hashset_collision_detection():
    l = [None] * 5
    hashinsert(l, 1)
    hashinsert(l, 6)
    assert hashcontains(l, 1)
    assert hashcontains(l, 6)

def test_aufg3_hashset_insert_twice():
    l = [None] * 5
    hashinsert(l, 1)
    assert hashcontains(l, 1)
    hashinsert(l, 1)
    assert hashcontains(l, 1)

def hashinsert(l, value):
    if len(l) == 0:
        l.append(value)
    else:
        pos = value % len(l)

        while l[pos] is not None:
            pos += 1
            if pos >= len(l):
                l.append(value)
                return

        l[pos] = value

def hashcontains(l, value):
    if len(l) == 0:
        return False

    pos = value % len(l)

    while l[pos] is not None:
        if l[pos] == value:
            return True
        pos += 1

    return False