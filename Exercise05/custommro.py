__author__ = 'das heck'

import math
import itertools

# Aufgabe 1

def flatten(lst):
    return sum(([x] if not isinstance(x, list) else flatten(x)
                for x in lst), [])


class Klass(object):
    def __init__(self, name, *parents):
        self.name = name
        self.parents = parents

        print(self.parents)

    def getparents(self):
        return self.parents

    def get_mro(self):
        print(self.name)
        print(self.parents)
        result = [self.name]

        for classes in self.parents:
            result.append(classes.get_mro())

        return flatten(result)


k1 = Klass("a")
k2 = Klass("b", k1)
k3 = Klass("c", k2, k1)
k4 = Klass("d")
k5 = Klass("e", k2, k4, k1)


def test_aufg1_simple():
    k1 = Klass("a")
    assert k1.get_mro() == [k1]
    k2 = Klass("b", k1)
    assert k2.get_mro() == [k2, k1]
    k3 = Klass("c", k2, k1)
    assert k3.get_mro() == [k3, k2, k1]
    k4 = Klass("d")
    assert k4.get_mro() == [k4]
    k5 = Klass("e", k2, k4, k1)
    assert k5.get_mro() == [k2, k1, k4]