import random, pygame, time, sys

title = "Game of Life in python"

# The framerate of the game (in milliseconds)
framerate = 60
screen_dimensions = (500, 500)

SQUARE = set([(0, 0), (0, 1),
              (1, 0), (1, 1)])
HLINE = set([(-1, 0), (0, 0), (1, 0)])
VLINE = set([(0, -1), (0, 0), (0, 1)])

GLIDER = set([       (0, -1),
                               (1, 0),
              (-1, 1), (0, 1), (1, 1)])

SHIFTED_GLIDER = set([(x + 1,y + 1) for (x, y) in GLIDER])

BRACKET = set([(0,0),(1,0),(2,0),(0,1),(2,1),(0,2),(2,2),(0,4),(2,4),(0,5),(2,5),(0,6),(1,6),(2,6)])

def lifestep(positionSet):
	if len(positionSet) == 0:
		return positionSet
		
	maxX = max(positionSet, key=lambda item:item[0])[0]
	maxY = max(positionSet, key=lambda item:item[1])[1]
	minX = min(positionSet, key=lambda item:item[0])[0]
	minY = min(positionSet, key=lambda item:item[1])[1]
	
	result = positionSet.copy()

	for x in range(minX-1,maxX+2):
		for y in range(minY-1,maxY+2):
			alive = (x,y) in positionSet
			numberOfAliveNeighbours = numberOfAdjacentAliveCells(positionSet, (x,y))	
			
			if alive == True:				
				if numberOfAliveNeighbours < 2 or numberOfAliveNeighbours > 3:
					result.remove((x,y))
			else:
				if numberOfAliveNeighbours == 3:
					result.add((x,y))
	
	return result

def numberOfAdjacentAliveCells(positionSet, position):
	counter = 0
	for x in range(position[0]-1,position[0]+2):
		for y in range(position[1]-1,position[1]+2):
			if (x,y) != position:
				if (x,y) in positionSet:
					counter += 1
	
	return counter
				
def lifestring(positionSet):
	if len(positionSet) == 0:
		return ""

	maxX = max(positionSet, key=lambda item:item[0])[0]
	maxY = max(positionSet, key=lambda item:item[1])[1]
	minX = min(positionSet, key=lambda item:item[0])[0]
	minY = min(positionSet, key=lambda item:item[1])[1]

	width = abs(maxX-minX)+1
	height = abs(maxY-minY)+1
	
	result = " " * (width * height)	
	temp = list(result)
	
	for position in positionSet:
		stringPos = (position[1]-minY)*width+(position[0]-minX)
		temp[stringPos] = "X"
	
	for i in range(0,height):
		temp.insert(i*width+width+i, "\n")
	
	result = "".join(temp)
		
	return result[0:-1]

def from_lifestring(string):
	result = set()
	
	if len(string) == 0:
		return result
		
	rows = string.split("\n")
	for i in range(len(rows)):		
		row = rows[i]
		index = row.find("X")
		
		while index >= 0:
			result.add((index,i))
			index = row.find("X", index+1)			
		
	return result	



	
g_maxX = -2**62
g_maxY = -2**64
g_minX = 2**64
g_minY = 2**64
	
def draw_field(cells, bg):
	if len(cells) == 0:
		return
				
	global g_maxX
	global g_maxY 
	global g_minX 
	global g_minY 
	maxX = max(cells, key=lambda item:item[0])[0]
	g_maxX = max(maxX, g_maxX)
	maxY = max(cells, key=lambda item:item[1])[1]
	g_maxY = max(maxY, g_maxY)
	minX = min(cells, key=lambda item:item[0])[0]
	g_minX = min(minX, g_minX)
	minY = min(cells, key=lambda item:item[1])[1]
	g_minY = min(minY, g_minY)
	
	sizeX = 500.0/(g_maxX-g_minX+1)
	sizeY = 500.0/(g_maxY-g_minY+1)
	nX = 500.0 - (sizeX*g_maxX)
	nY = 500.0 - (sizeY*g_maxY)
	
	pygame.draw.rect(bg, (0,0,0), (0,0,500,500))
	
	for pos in cells:
		posX = sizeX*(pos[0]-1)+nX
		posY = sizeY*(pos[1]-1)+nY
		pygame.draw.rect(bg, (255,255,255), (posX,posY,sizeX,sizeY))

def load_initial_state(filename):
	with open(filename) as f:
		s = f.read()
		return from_lifestring(s)		
		
def main():

    # Initialize pygame elements
	screen, bg, clock = init_screen()
	
	global start
	
    # Even loop
	while True:
		clock.tick(framerate)

        # Draw some random rectangles:
		start = lifestep(start)
		draw_field(start, bg)		
		
        # draw the background screen to the actual one
		screen.blit(bg, (0,0))
        # update the screen
		pygame.display.flip()

		time.sleep(0.1)
		
        # look out for window close events:
		for e in pygame.event.get():
			if e.type == pygame.QUIT:
				return

def init_screen():

	pygame.init()
	screen = pygame.display.set_mode(screen_dimensions)
	pygame.display.set_caption(title)
	bg = pygame.Surface(screen_dimensions)
	clock = pygame.time.Clock()
	return screen, bg, clock


	
if(len(sys.argv) >= 2):
	start = load_initial_state(sys.argv[1])
else:
	start = BRACKET
	
main()

