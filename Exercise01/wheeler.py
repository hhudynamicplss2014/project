def burrows_wheeler_forward(text):
	array = []
	for i in range(0,len(text)):
		array.append(text[i:len(text)] + text[0:i])
	
	array.sort()
	print(array)
	result = ""
	
	for i in range(0,len(array)):
		result += array[i][-1]
		
	return (result, array.index(text))	

print(burrows_wheeler_forward("TEXTUEL"))	
#print(burrows_wheeler_forward("TRENTATRE.TRENTINI.ANDARONO.A.TRENTO.TUTTI.E.TRENTATRE.TROTTERELLANDO"))

def burrows_wheeler_backward(text, index):
	array = []
	for i in range(0,len(text)):
		array.append(text[i] + str(-1 if i == index else i))
		
	array.sort(key = lambda s: s[0])
	#print(array)
	
	result = ""	
	while index >= 0:
		result += array[index][0]
		index = int(array[index][1:])
	
	return result	
		
print(burrows_wheeler_backward("a!iepdWkii", 1))		
print(burrows_wheeler_backward("UTELXTE", 3))		
print(burrows_wheeler_backward("OIIEEAEO..LDTTNN.RRRRRRRTNTTLEAAIOEEEENTRDRTTETTTTATNNTTNNAAO....OU.T", 60))## 

f = open('wheeler.py', 'r')
text = f.read()
f.close()
(transformed, index) = burrows_wheeler_forward(text)
output = burrows_wheeler_backward(transformed, index)
print(output)