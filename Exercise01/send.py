import py
import os

class A(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def f(self, z):
        return self.x + self.y + z


class B(A):
    def g(self):
        return self.x * 17


def test_send():
    a = A(12, 13)
    assert a.f(1) == 26
    assert send(a, "f", 1) == 26

    b = B(14, 15)
    assert b.f(-1) == 28
    assert send(b, "f", -1) == 28
    assert b.g() == 14 * 17
    assert send(b, "g") == 14 * 17


def test_send_not_found():
    a = A(12, 13)
    py.test.raises(AttributeError, "a.notthere(1)")
    py.test.raises(AttributeError, "send(a, 'notthere', 1)")


class C(A):
    def message_not_understood(self, message, *args):
        return ('we have seen', message, args)


def test_send_message_not_understood():
    c = C(16, 17)
    assert send(c, 'notthere', 1) == ('we have seen', 'notthere', (1,))

def send(obj, message, *args):
	cls = obj.__class__
	while cls is not None:
		if message in cls.__dict__:
			return cls.__dict__[message](obj, *args)
		cls = cls.__base__
		
	if "message_not_understood" in obj.__class__.__dict__:
		return obj.__class__.__dict__["message_not_understood"](obj, message, *args)
	else:
		raise AttributeError()	
		