EPSILON = 0.0001

def find_zero(f, a, b):
	if f(a) == 0:
		return a
	
	if f(b) == 0:
		return b

	if abs(b-a) <= EPSILON:
		return b
		
	m = (a+b)/2
	
	if abs(f(m)) == 0:
		return m
	else:
		if f(m)*f(a) < 0:
			return find_zero(f, a, m)
		else:
			if f(m)*f(b) < 0:
				return find_zero(f, m, b)
			else:
				raise ValueError('Could not find zero for f in [' + str(a) + ',' + str(b) + ']')
				

from math import *			

def f(x):
	return (x-1)*x

print(find_zero(math.cos, 0, 2))
print(find_zero(f, 0.5, 100))
print(find_zero(f, -100, 0.5))
print(find_zero(math.cos, 2, 0))
#print(find_zero(math.cos, 0, 0.5))


def approximate_sqrt(x):
	def f(arg):
		return (arg*arg) - x
		
	return find_zero(f, 0, x+1)

print(approximate_sqrt(0))
	
	
    #assert abs(res - math.pi / 2.0) <= EPSILON
	
hidden_value = "this is secret" 
#def dangerous_function(filename): 
#	print open(filename).read() 
	
#make a list of safe functions 
safe_list = ['math','acos', 'asin', 'atan', 'atan2', 'ceil', 'cos', 'cosh', 'de grees', 'e', 'exp', 'fabs', 'floor', 'fmod', 'frexp', 'hypot', 'ldexp', 'log', 'log10', 'modf', 'pi', 'pow', 'radians', 'sin', 'sinh', 'sqrt', 'tan', 'tanh'] 

#use the list to filter the local namespace 
safe_dict = dict([ (k, locals().get(k, None)) for k in safe_list ]) 
#add any needed builtins back in. 
safe_dict['abs'] = abs 

#user_func = raw_input("type a function: y = ") 
print("teste")
print(safe_dict)

for x in range(1,10): # add x in 
	safe_dict['x']=x 
	print("x = ", x , ", y = ", eval("sin(x)",{"__builtins__":None},safe_dict))