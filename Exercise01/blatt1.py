import py
import os

# ____________________________________________________________
#
# AUFGABE 1
#
def test_count_lines_and_words():
    try:
        f = open("test.txt", "w")
        f.write("a bc def ghi\n" * 16)
        f.close()
        lines, words = count_lines_and_words('test.txt')
        assert lines == 16
        assert words == 4 * 16
    finally:
        os.unlink("test.txt")

#
# Opens the file with the given filename and returns
# the number of lines and words in it.
#		
def count_lines_and_words(filename):	
	#with open(filename, WrW) as f 
	#	do_stuff_with(f) ...
	
	f = open(filename, "r")
	lines = 0
	words = 0

	for line in f:
		lines += 1
		words += len(line.split())

	f.close() 
	return (lines,words)

# ____________________________________________________________
#
# AUFGABE 2
#
import math

EPSILON = 0.0001

def test_find_zero():
    res = find_zero(math.cos, 0, 2)
    assert abs(res - math.pi / 2.0) <= EPSILON

    def f(x):
        return (x - 1) * x

    res = find_zero(f, 0.5, 100)
    assert abs(res - 1.0) <= EPSILON

    res = find_zero(f, -100, 0.5)
    assert abs(res - 0.0) <= EPSILON

def test_initial_condition():
    py.test.raises(ValueError, find_zero, math.cos, 0, 0.5)


def test_reversed_a_and_b():
    res = find_zero(math.cos, 2, 0)
    assert abs(res - math.pi / 2.0) <= EPSILON

#
# Finds the argument x, such that f(x) = 0 by using the bisection method.
#
def find_zero(f, a, b):
	if f(a) == 0:
		return a
	
	if f(b) == 0:
		return b

	if abs(b-a) <= EPSILON:
		return b
		
	m = (a+b)/2
	
	if abs(f(m)) == 0:
		return m
	else:
		if f(m)*f(a) < 0:
			return find_zero(f, a, m)
		else:
			if f(m)*f(b) < 0:
				return find_zero(f, m, b)
			else:
				raise ValueError('Could not find zero for f in [' + str(a) + ',' + str(b) + ']')

def test_sqrt():
    for num in [0, 0.2, 0.5, 1, 10, 41, 100]:
        assert abs(approximate_sqrt(num) - math.sqrt(num)) <= EPSILON

# 
# Finds the square root of an argument a by finding the zero value of the function 0 = f(x) = x^2-a		
def approximate_sqrt(x):
	def f(arg):
		return (arg*arg) - x
		
	return find_zero(f, 0, math.sqrt(x)+1)

def test_make_function():
    # basic case
    f = make_function("x * 6")
    assert f(7) == 42

    # extended version: functions from the math module can be used too
    f = make_function("sin(x) - sqrt(x)")
    assert f(1.2) == math.sin(1.2) - math.sqrt(1.2)

def test_find_zero_from_string():
    # this should be just find_zero combined with make_function
    res = find_zero_from_string("cos(x)", 0, 2)
    assert abs(res - math.pi / 2.0) <= EPSILON

#	
# Transforms a string expression to a function by evaluating the string
def make_function(expr):
	import math	
	def f(y):		
		return eval(expr, math.__dict__, {"x": y})
		
	return f
	
#
# Finds the zero value from a function given by a string expression	
def find_zero_from_string(expr, a, b):
	return find_zero(make_function(expr), a, b)
	
# ____________________________________________________________
#
# AUFGABE 3
#

class A(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def f(self, z):
        return self.x + self.y + z


class B(A):
    def g(self):
        return self.x * 17


def test_send():
    a = A(12, 13)
    assert a.f(1) == 26
    assert send(a, "f", 1) == 26

    b = B(14, 15)
    assert b.f(-1) == 28
    assert send(b, "f", -1) == 28
    assert b.g() == 14 * 17
    assert send(b, "g") == 14 * 17


def test_send_not_found():
    a = A(12, 13)
    py.test.raises(AttributeError, "a.notthere(1)")
    py.test.raises(AttributeError, "send(a, 'notthere', 1)")


class C(A):
    def message_not_understood(self, message, *args):
        return ('we have seen', message, args)


def test_send_message_not_understood():
    c = C(16, 17)
    assert send(c, 'notthere', 1) == ('we have seen', 'notthere', (1,))


def send(obj, message, *args):
	cls = obj.__class__
	while cls is not None:
		if message in cls.__dict__:
			return cls.__dict__[message](obj, *args)
		cls = cls.__base__
		
	if "message_not_understood" in obj.__class__.__dict__:
		return obj.__class__.__dict__["message_not_understood"](obj, message, *args)
	else:
		raise AttributeError()		
	
# ____________________________________________________________
#
# AUFGABE 4
#

def test_forward_transformation():
    # see http://de.wikipedia.org/wiki/Burrows-Wheeler-Transformation
    assert burrows_wheeler_forward("TEXTUEL") == ("UTELXTE", 3)

    input = ("TRENTATRE.TRENTINI.ANDARONO.A.TRENTO.TUTTI.E.TRENTATRE.TROTTERELLANDO")
    expected_output = ("OIIEEAEO..LDTTNN.RRRRRRRTNTTLEAAIOEEEENTRDRTTETTTTATNNTTNNAAO....OU.T")
    assert burrows_wheeler_forward(input) == (expected_output, 60)

#
# Does a burrows wheeler transformation, where a text is encrypted	
def burrows_wheeler_forward(text):
	array = []
	for i in range(0,len(text)):
		array.append(text[i:len(text)] + text[0:i])
	
	array.sort()
	result = ""
	#string konkatention erzeugt jeweils kopie von string -> sehr zeitkomplex
	#stattdessen listen benutzen
	
	#for i in range(0, len(array)):
	#	resultArray.append(array[i][-1])
	#
	#result = "".join(resultArray)
	
	for i in range(0,len(array)):
		result += array[i][-1]
		
	return (result, array.index(text))		
	

def test_backward_transformation():
	assert burrows_wheeler_backward("UTELXTE", 3) == "TEXTUEL"
	
	expected_output = ("TRENTATRE.TRENTINI.ANDARONO.A.TRENTO.TUTTI.E.TRENTATRE.TROTTERELLANDO")
	input = ("OIIEEAEO..LDTTNN.RRRRRRRTNTTLEAAIOEEEENTRDRTTETTTTATNNTTNNAAO....OU.T")

	assert burrows_wheeler_backward(input, 60) == expected_output

#
# Does a burrows wheeler transformation, where a crypted text and an index are decrypted.
# We concatenate the char and the position on which it is parsed (e.g. the char "a" at position 4 results in the string "a4").
# To stabilise the sorting algorithm we only consider the first char of the string, when sorting.		
def burrows_wheeler_backward(text, index):
	array = []
	for i in range(0,len(text)):
		array.append(text[i] + str(-1 if i == index else i))
		
	array.sort(key = lambda s: s[0])
	#print(array)
	
	result = ""	
	while index >= 0:
		result += array[index][0]
		index = int(array[index][1:])
	
	return result
	
def test_composition():
    f = open('blatt1.py', 'r')
    text = f.read()
    f.close()
    (transformed, index) = burrows_wheeler_forward(text)
    output = burrows_wheeler_backward(transformed, index)
    assert output == text
