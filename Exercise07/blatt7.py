import test

class ProtoMeta:
    def __new__(cls, name, bases, attrs):
        return ProtoObject(attrs)
        #attrs is the class body

def clone():
    return ProtoObject({ "parent" : None })


class ProtoObject(object):
    def __init__(self, attr_hash):
        #print("Initing new object with " + str(attr_hash))

        for key in attr_hash:
            if key == 'parent':
                if attr_hash[key] is not None and attr_hash[key].__class__ is not ProtoObject:
                    raise AttributeError('Wrong type for parent.')

            self.__dict__[key] = attr_hash[key]

    def __getattr__(self, item):
        if item == 'parent':
            return default_parent

        if self.parent is None:
            raise AttributeError(item + ' was not found in prototype hierarchy.')
        else:
            return getattr(self.parent, item)

    def __setattr__(self, key, value):
        if key == "parent":
            self.__dict__["parent"] = value
        else:
            if not self.is_in_hierarchy(key) or self.is_in_object(key):
               self.__dict__[key] = value
            else:
                if self.parent is not None:
                    setattr(self.parent, key, value)

    def is_in_object(self, item):
        return self.__dict__.__contains__(item)

    def is_in_hierarchy(self, item):
        if self.parent is not None:
            return self.parent.is_in_object(item)
        else:
            return False

default_parent = ProtoObject({"parent" : None, "clone" : clone})

x1 = ProtoObject({"a": 1})
y = ProtoObject({"b": 1, "parent": x1})
x2 = ProtoObject({"c": 2})

class x5():
    __metaclass__ = ProtoMeta
    a = 1
    b = 2

def test_simple():
    x1 = ProtoObject({"a": 1})
    assert x1.a == 1
    x1.a = 2
    assert x1.a == 2
    assert x1.parent is default_parent
    assert default_parent.parent is None

    y = ProtoObject({"b": 1, "parent": x1})
    assert y.b == 1
    assert y.a == 2
    y.a = 3
    assert y.a == 3
    assert x1.a == 3

    x2 = ProtoObject({"c": 41})
    y.parent = x2
    assert y.b == 1
    assert y.c == 41
    py.test.raises(AttributeError, "y.a")


def test_write():
    x1 = ProtoObject({"a": 1})
    x2 = ProtoObject({"b": 1, "parent": x1})
    assert x2.a == 1
    x2.a = 2
    assert x1.a == 2
    assert x2.a == 2
    x2.b = 2
    assert x2.b == 2
    py.test.raises(AttributeError, "x1.b")
    x2.c = 1
    assert x2.c == 1
    py.test.raises(AttributeError, "x1.c")


def test_syntax():
    class x1():
        __metaclass__ = ProtoMeta
        a = 1

    assert isinstance(x1, ProtoObject)
    assert x1.a == 1
    x1.a = 2
    assert x1.a == 2

    class y():
        __metaclass__ = ProtoMeta
        b = 1
        parent = x1

    assert isinstance(y, ProtoObject)
    assert y.b == 1
    assert y.a == 2
    y.a = 3
    assert y.a == 3
    assert x1.a == 3

    class x2():
        __metaclass__ = ProtoMeta
        c = 41

    y.parent = x2
    assert y.b == 1
    assert y.c == 41
    py.test.raises(AttributeError, "y.a")


def test_method():
    class x1():
        __metaclass__ = ProtoMeta
        a = 1
        def f(self, x):
            return self.a + x
    assert x1.f(10) == 11
    x1.a = 34
    assert x1.f(10) == 44

    class y():
        __metaclass__ = ProtoMeta
        a = 2
        parent = x1

    assert y.f(10) == 12
    f = y.f
    assert f(11) == 13
    y.a = 34
    assert y.f(10) == 44


def test_clone():
    empty = default_parent.clone()
    assert empty is not default_parent
    assert empty.parent is None

    class x1():
        __metaclass__ = ProtoMeta
        a = 1
    x2 = x1.clone()
    assert x2.a == 1
    x2.a = 34
    assert x2.a == 34
    assert x1.a == 1


def test_override_clone():

    class x1():
        __metaclass__ = ProtoMeta
        distance = 1
        a = 23
        def clone(self):
            class cloned():
                __metaclass__ = ProtoMeta
                distance = self.distance + 1
                a = self.a
            return cloned

    x2 = x1.clone()
    assert x1 is not x2
    assert x2.distance == 2
    assert x2.a == 23
    x2.a = 24
    assert x2.a == 24
    assert x1.a == 23

    # default clone is used here
    x3 = x2.clone()
    assert x3.distance == 2
    assert x3.a == 24
