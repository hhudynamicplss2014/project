# README #

This is the semester long project of an extended parser for a custom language called simple. It is implemented in python by 

* Stefan Neidig (1972470 - Stefan.Neidig@uni-duesseldorf.de)
* Rene Goebbels (1934395 - rene.goebbels@hhu.de)

### What is this repository for? ###

This summary contains besides the homework of the semester the project, which is located in "./Project". 

Its current version is 1.0.0 final.

### How do I get set up? ###

First you would clone the repository by running 
```
#!bash
git clone git@bitbucket.org:hhudynamicplss2014/project.git
```

There is nothing more you have to do besides having Python 2.7 and py.test installed. Then you may checkout some of the examples in ./Project/examples.py. It would also possibly a good idea to change the directory to "./Project". To start the tests run the following command 

```
#!bash
py.test

```
There is also a documentation located in "./Projects/tex", which contains two articles, describing the 'why' and 'how' of the new feature(s).

The first article (explanation.pdf) describes the new syntax and explains why we chose the path, we had walked over the past months. The second one describes the implementation of the features from three aspects - object model, parser and interpreter.
### Who do I talk to? ###

For any recommandation, issues or bugx please contact one of the contributers listed above.