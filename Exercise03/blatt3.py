class myclassmethod(object):
	def __init__(self,f):
		self.f = f
		
	def __get__(self, obj, klass=None):
		if klass is None:
		   klass = type(obj)
		def newfunc(*args):
		   return self.f(klass, *args)
		return newfunc
		
class mystaticmethod(object):
	def __init__(self, getter):
		self.getter = getter
		
	def __get__(self, inst, cls):
		return self.getter	

def test_classmethod():
    class A(object):
        i = 0

		#@myclassmethod decorator, verkürzte schreibweise von f = myclassmethod(f). Decorator bezieht sich immer auf nachfolgende Funktion
        def f(cls):
            cls.i += 1
        f = myclassmethod(f)

    A.f()
    assert A.i == 1
    a = A()
    a.f()
    assert A.i == 2
    assert a.i == 2
    a.i = 'a'
    a.f()
    assert A.i == 3
    assert a.i == 'a'


def test_staticmethod():
    class A(object):
        #@mystaticmethod
		def f(i):
            return i
        f = mystaticmethod(f)

    assert A.f('a') == 'a'
    a = A()
    assert a.f(34) == 34
    assert a.f(a) is a

# ____________________________________________________________
# AUFGABE 2


SQUARE = set([(0, 0), (0, 1),
              (1, 0), (1, 1)])
HLINE = set([(-1, 0), (0, 0), (1, 0)])
VLINE = set([(0, -1), (0, 0), (0, 1)])

GLIDER = set([       (0, -1),
                               (1, 0),
              (-1, 1), (0, 1), (1, 1)])

SHIFTED_GLIDER = set([(x + 1,y + 1) for (x, y) in GLIDER])


def test_life_step():
    assert lifestep(set()) == set()
    assert lifestep(set([(0, 0)])) == set()

    assert lifestep(SQUARE) == SQUARE
    assert lifestep(set([(0, 0), (0, 1),
                         (1, 0)       ])) == SQUARE

    assert lifestep(HLINE) == VLINE
    assert lifestep(VLINE) == HLINE

    assert lifestep(lifestep(lifestep(lifestep(GLIDER)))) == SHIFTED_GLIDER


def test_life_string():
    assert lifestring(set()) == ""
    assert lifestring(set([(0, 0)])) == "X"
    assert lifestring(set([(10, -5),(12, -5)])) == "X X"

    assert lifestring(SQUARE) == "XX\nXX"    # '\n' is an end-of-line character
    assert lifestring(set([(0, 0), (0, 1),
                           (1, 0)       ])) == "XX\nX "

    assert lifestring(HLINE) == "XXX"
    assert lifestring(VLINE) == "X\nX\nX"

    assert lifestring(GLIDER) == (" X \n" +
                                  "  X\n" +
                                  "XXX")
    assert lifestring(SHIFTED_GLIDER) == lifestring(GLIDER)


# def test_life_big():
    # import time
    # state = set([        (0, -1), (1, -1),
                 # (-1, 0), (0, 0),
                          # (0, 1)          ])
    # for i in range(1103):
        # # for fun, print the first 100 iterations
        # if i < 200:
            # print('-'*80)
            # print(lifestring(state))
            # time.sleep(0.1)
        # state = lifestep(state)
    # resulting_string = lifestring(state)    # big! about 500x500...

    # import hashlib
    # h = hashlib.md5(resulting_string).hexdigest()
    # assert h == '274cb707b8579bb5a0efbc1b56b0de59'

	
def test_life_semi_big():
    import time
    state = set([        (0, -1), (1, -1),
                 (-1, 0), (0, 0),
                          (0, 1)          ])
    for i in range(200):
        # for fun, print the first 100 iterations
        #if i < 50:
            #print('-'*80)
            #print(lifestring(state))
            #time.sleep(0.1)
        state = lifestep(state)
    resulting_string = lifestring(state)    # big! about 500x500...

    import hashlib
    h = hashlib.md5(resulting_string.encode('utf-8')).hexdigest()
    assert h == '845438e54d332578d6d1fdcc40b394de'

def lifestring(positionSet):
	if len(positionSet) == 0:
		return ""

	maxX = max(positionSet, key=lambda item:item[0])[0]
	maxY = max(positionSet, key=lambda item:item[1])[1]
	minX = min(positionSet, key=lambda item:item[0])[0]
	minY = min(positionSet, key=lambda item:item[1])[1]

	width = abs(maxX-minX)+1
	height = abs(maxY-minY)+1
	
	result = " " * (width * height)	
	temp = list(result)
	
	for position in positionSet:
		stringPos = (position[1]-minY)*width+(position[0]-minX)
		temp[stringPos] = "X"
	
	for i in range(0,height):
		temp.insert(i*width+width+i, "\n")
	
	result = "".join(temp)
		
	return result[0:-1]

def lifestep(positionSet):
	if len(positionSet) == 0:
		return positionSet
		
	maxX = max(positionSet, key=lambda item:item[0])[0]
	maxY = max(positionSet, key=lambda item:item[1])[1]
	minX = min(positionSet, key=lambda item:item[0])[0]
	minY = min(positionSet, key=lambda item:item[1])[1]
	
	result = positionSet.copy()

	for x in range(minX-1,maxX+2):
		for y in range(minY-1,maxY+2):
			alive = (x,y) in positionSet
			numberOfAliveNeighbours = numberOfAdjacentAliveCells(positionSet, (x,y))	
			
			if alive == True:				
				if numberOfAliveNeighbours < 2 or numberOfAliveNeighbours > 3:
					result.remove((x,y))
			else:
				if numberOfAliveNeighbours == 3:
					result.add((x,y))
	
	return result

def numberOfAdjacentAliveCells(positionSet, position):
	counter = 0
	for x in range(position[0]-1,position[0]+2):
		for y in range(position[1]-1,position[1]+2):
			if (x,y) != position:
				if (x,y) in positionSet:
					counter += 1
	
	return counter