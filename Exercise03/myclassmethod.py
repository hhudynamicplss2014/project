class myclassmethod(object):
	def __init__(self,f):
		self.f = f
		
	def __get__(self, obj, klass=None):
		if klass is None:
		   klass = type(obj)
		def newfunc(*args):
		   return self.f(klass, *args)
		return newfunc
		
class A(object):
	i = 0
	
	def f(cls):
		cls.i += 1
		
	f = myclassmethod(f)
	
def test_classmethod():
    class A(object):
        i = 0

        def f(cls):
            cls.i += 1
        f = myclassmethod(f)

    A.f()
    assert A.i == 1
    a = A()
    a.f()
    assert A.i == 2
    assert a.i == 2
    a.i = 'a'
    a.f()
    assert A.i == 3
    assert a.i == 'a'
	
class mystaticmethod(object):
	def __init__(self, getter):
		self.getter = getter
		
	def __get__(self, inst, cls):
		return self.getter	
	
class B(object):
	def f(i):
		return i

	f = mystaticmethod(f)
	

	
def test_staticmethod():
    class A(object):
        def f(i):
            return i
        f = mystaticmethod(f)

    assert A.f('a') == 'a'
    a = A()
    assert a.f(34) == 34
    assert a.f(a) is a