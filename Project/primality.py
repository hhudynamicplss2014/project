__author__ = 'das heck'

from interpreter import *

def test_primality():
    ast = ("""
def prim(n):
    start = n sub(1)
    isprim = true
    while start gt(1):
        res = n mod(start)
        start = start sub(1)
        if res eq(0):
            isprim = false

    isprim


a = prim(53)
b = prim(29)
c = prim(10)
    """)

    interpreter = Interpreter()
    interpreter.debug = False
    w_module = interpreter.make_module()
    interpreter.eval(parse(ast), w_module)

    assert w_module.getvalue("a").value == 1
    assert w_module.getvalue("b").value == 1
    assert w_module.getvalue("c").value == 0

def test_primality_in_self():
    ast = ("""
def prim(n):
    start = n sub(1)
    isprim = true
    while start gt(1):
        res = n mod(start)
        start = start sub(1)
        if res eq(0):
            isprim = false
    isprim

def testprim:
    a = prim(53)
    b = prim(29)
    c = prim(10)

    result = false

    if a eq(true):
        if b eq(true):
            if c eq(false):
                result = true

    result

test = testprim
    """)

    interpreter = Interpreter()
    interpreter.debug = False
    w_module = interpreter.make_module()
    interpreter.eval(parse(ast), w_module)

    assert w_module.getvalue("test").value == 1

