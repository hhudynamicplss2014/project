__author__ = 'das heck'

from interpreter import *

"""
    Here are some examples how to use the new features of simple. They are listed as hash, for a more comfortable usage.
    Just run dump_all_to_file with a filename as argument. It will produce a file, which dumps the global object, which
    we used to call w_module and all of its values and subvalues.

    To run a single program, run execute with the programs name. Example: execute('simple_break')
"""

programs = {
# break
"simple_break" : """
i = 0
while i lt(20):
    i = i add(1)
    if i eq(10):
        break
"""
,
#continue
"simple_continue" : """
i = 0
a = 0
while i lt(20):
    i = i add(1)
    continue
    a = a sub(2)
"""
,
#return
"simple_return" : """
def f(a):
    a = a mul(10)
    return a

first = f(10)
second = f(20)
"""
,
"return_no_value" : """
def none:
    return

a = none
b = none
"""
,
"return_from_loop" : """
def func(a):
    while true:
        if a eq(1):
            return a

k = func(1)
"""
,
"fibonacci" : """
def fib(n):
    counter = 0
    a = 0
    b = 1
    while counter lt(n):
        counter = counter add(1)

        old_a = a
        old_b = b

        a = old_b
        b = old_a add(old_b)

    return a

t = fib(10)
"""
,
"fibonacci_recursive" : """
def fib(n):
    if n eq(0):
        return 0
    else:
        if n leq(2):
            return 1
        else:
            a = self fib(n sub(1))
            b = self fib(n sub(2))
            result = a add(b)

            return result

t = fib(10)
"""
,
#exception
"simple_exception" : """
def my_div(a,b):
    if b eq(0):
        raise exception 1024

begin:
    result = my_div(10,0)
rescue exception:
    error = exception error_number
"""
,
"builtin_exception_1" : """
def f:
    return

begin:
    a = 20 add(f)
rescue WrongTypeException:
    error = 123456789
"""
,
"builtin_exception_2" : """
def f:
    return

begin:
    a = f add(20)
rescue MethodCallOnNoneException:
    error = 123456789
"""
,
"builtin_exception_3" : """
begin:
    a = 10 div(0)
rescue ZeroDivisionException:
    error = 123456789
"""
,
"builtin_exception_4" : """
object obj:
    a = 0

begin:
    obj missing
rescue MethodMissingException:
    result = 123456789
"""
}

def dump_all_to_file(filename):
    file = open(filename, "w")

    for key in programs:
        file.write(execute(key))
        file.write("\n")

    file.close()


def execute(program):
    if programs.has_key(program):
        result = "Calling the following example:\n"
        result += programs[program] + "\n"

        interpreter = Interpreter()
        w_module = interpreter.make_module()
        interpreter.eval(parse(programs[program]), w_module)

        result += "The result is:\n\n"
        result += w_module.dump()
        result += "\n" + ("*"*80)

        return result

    return "Could not find the program \"" + str(program) + "\""