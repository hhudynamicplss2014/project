__author__ = 'das heck'
__editor__= 'Rene'
from objmodel import W_Exception, W_Integer, W_NormalObject, W_None

"""
    Here we may cause some builtin exceptions, when something goes wrong regarding builtin primitives. We call this
    builtin exceptions. There are currently four known builtin exceptions:

    1) WrongTypeException
        This exception is called, when the actual type do not match the expected ones. We added a decorator called
        'primitive' to each primitive method, which describes as second parameter the expected parameter types as
        array. They get checked and verified in the closure g, which is defined in unwrapper. If something goes wrong,
        an exception WrongTypeException gets thrown. The error_number indicates, which expected type did not match.

        error_number = 0 : An int (W_Integer) type was expected but not found.
        error_number = 1 : A none (W_None) type was expected but not found.

    2) ZeroDivisonException
        This exception is called, when one tries to divide by zero. It can be triggered in the primitive methods 'div'
        and 'mod', which also does a division internally. The error_number is always -1.

    3) MethodCallOnNoneException
        This exception is called, when one would try to call a method on a W_None type value. This may occur by calling
        a method on a return value, which might be W_None, when no return value is provided. Due to the structure of
        the interpreter this exception is called in the method eval_MethodCall. The error_number is always 0.

    4) MethodMissingException
        This exception is called, when a method called upon an object is missing. This might be because you missspelled
        the methods name or the name of the receiver. However the error number of this very exception is -1.
"""

registry = {  }


def unwrapper(f, args, ret):
    def g(*args_w):
        unwrapped_args = []
        assert len(args) == len(args_w)

        for i,x in enumerate(args):
            if x == 'int':
                if not isinstance(args_w[i], W_Integer):
                    return W_Exception(name = "WrongTypeException", error_number = 0)
                else:
                    unwrapped_args.append(args_w[i].value)
            elif x == 'none':
                if not isinstance(args_w[i], W_None):
                    return W_Exception(name = "WrongTypeException", error_number = 1)
                else:
                    unwrapped_args.append(None)

        ret_val = f(unwrapped_args[0], unwrapped_args[1])

        if isinstance(ret_val, W_Exception):
            return ret_val

        if ret == 'int':
            return W_Integer(ret_val)
    return g


def primitive(name, args, ret):
    def decorator(f):
        registry["$int_" + name] = unwrapper(f, args, ret)
        return f
    return decorator

@primitive('add', ['int', 'int'], 'int')
def __int_add(a,b):
    return a + b


@primitive('sub', ['int', 'int'], 'int')
def __int_sub(a,b):
    return a - b


@primitive('mul', ['int', 'int'], 'int')
def __int_mul(a,b):
    return a * b


@primitive('div', ['int', 'int'], 'int')
def __int_div(a,b):
    if b == 0:
        return W_Exception(name = "ZeroDivisionException", error_number = -1)

    return a // b


@primitive('mod', ['int', 'int'], 'int')
def __int_mod(a,b):
    if b == 0:
        return W_Exception(name = "ZeroDivisionException", error_number = -1)

    return a % b


@primitive('lt', ['int', 'int'], 'int')
def __int_lt(a,b):
    return int(a < b)


@primitive('le', ['int', 'int'], 'int')
def __int_le(a,b):
    return int(a <= b)


@primitive('gt', ['int', 'int'], 'int')
def __int_gt(a,b):
    return int(a > b)


@primitive('ge', ['int', 'int'], 'int')
def __int_ge(a,b):
    return int(a >= b)


@primitive('eq', ['int', 'int'], 'int')
def __int_eq(a,b):
    return int(a == b)


@primitive('neq', ['int', 'int'], 'int')
def __int_neq(a,b):
    return int(a != b)