from test_helper import *


def test_raise_rescue_all():
    prog = """
begin:
    raise MyException
rescue Exception:
    result = 1
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 1


def test_raise_rescue_named():
    prog = """
begin:
    raise MyException
rescue MyException:
    result = 1
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 1


def test_raise_rescue_first():
    prog = """
begin:
    raise first
rescue first:
    result = 1
rescue second:
    result = 2
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 1


def test_raise_rescue_second():
    prog = """
begin:
    raise second
rescue first:
    result = 1
rescue second:
    result = 2
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 2


def test_raise_interrupts():
    prog = """
begin:
    x = 1
    raise first
    x = 2
    y = 0
rescue first:
    result = 1
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("x").value == 1
    assert w_module.getvalue("result").value == 1
    assert w_module.getvalue("y") is None


def test_primitive_exception():
    prog = """
begin:
    o = 6
    o = o div(0)
rescue ZeroDivisionException:
    result = ZeroDivisionException error_number
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("o").value == 6
    assert w_module.getvalue("result").value == -1


def test_exception_with_no_error_number_returns_w_none():
    prog = """
begin:
    raise error
rescue error:
    result = error error_number
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert isinstance(w_module.getvalue("result"), W_None)


def test_primitive_exception_not_rescued():
    prog = """
o = 6
o = o div(0)
# the following gets not executed
y = 0
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("o").value == 6
    assert w_module.getvalue("y") is None


def test_multiple_exceptions_parallel():
    prog = """
def f(a,b):
    if a eq(0):
        raise first
    if b eq(0):
        raise second

result = 0

begin:
    f(0,1)
    f(1,0)
rescue first:
    result = result add(1)
rescue second:
    result = result add(2)
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 1


def test_multiple_exceptions_sequence():
    prog = """
result = 0

begin:
    begin:
        raise second
    rescue Exception:
        result = result add(1)
        a = 0

    raise first
rescue Exception:
    result = result add(1)
    b = 0
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 2
    assert w_module.getvalue("b").value == 0
    assert w_module.getvalue("a").value == 0


def test_do_not_catch_exceptions_twice_but_use_the_first_only():
    prog = """
a = 0
begin:
    raise first
rescue first:
    a = a add(1)
rescue first:
    a = a add(2)
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("a").value == 1


def test_do_not_catch_all_when_allready_caught():
    prog = """
begin:
    raise first
rescue first:
    result = 1
rescue Exception:
    result = 2
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 1


def test_statements_after_rescue_must_be_executed():
    prog = """
begin:
    raise first
rescue Exception:
    result = 0

a = 0
b = 0
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 0
    assert w_module.getvalue("a").value == 0
    assert w_module.getvalue("b").value == 0


def test_statements_after_exception_must_not_be_executed():
    prog = """
begin:
    raise first
rescue second:
    result = 1

a = 0
b = 0
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result") is None
    assert w_module.getvalue("a") is None
    assert w_module.getvalue("b") is None


def test_nested_unhandled_exception():
    prog = """
begin:
    begin:
        raise first
    rescue second:
        result = 0
rescue Exception:
    result = 1
"""

    w_module = parse_ast_and_return_w_module(prog)
    assert w_module.getvalue("result").value == 1


def test_exception_must_have_access_to_context():
    prog = """
a = 0
c = 0
begin:
    b = 0
    raise exc
rescue Exception:
    c = c add(20)
    a = a add(1)
    b = b add(10)
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("a").value == 1
    assert w_module.getvalue("b").value == 10
    assert w_module.getvalue("c").value == 20

def test_accessing_global_variable_in_begin():
    ast = """
def f(a):
    a

temp = 0

begin:
    temp = f(5)
rescue Exception:
    flag = 0
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("temp").value == 5


def test_exception_raised_in_while_loop():
    prog = """
begin:
    o = 6
    a = 0
    if a eq(0):
        raise ZeroDivisionException 1
    else:
        result = o div(a)

rescue ZeroDivisionException:
    result = ZeroDivisionException error_number
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("result").value == 1


def test_implicit_exception_in_while():
    prog = """
a = 10
begin:
    while a:
        b = a sub(2)
        temp = a mod(b)
        a = a sub(1)

rescue ZeroDivisionException:
    result = ZeroDivisionException error_number
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("a").value == 2
    assert w_module.getvalue("result").value == -1

def test_implicit_exception_in_if():
    prog = """
a = 2
begin:
    if a:
        a = 5 div(0)
rescue ZeroDivisionException:
    result = ZeroDivisionException error_number
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("a").value == 2
    assert w_module.getvalue("result").value == -1


def test_custom_error_number():
    prog = """
begin:
    raise error 20
rescue error:
    result = error error_number
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("result").value == 20


def test_nested_begin():
    prog = """
begin:
    a = 0
    begin:
        if a eq(0):
            raise error 10
    rescue error:
        result = error error_number
rescue Exception:
    result = 0
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("result").value == 10


def test_nested_rescue():
    prog = """
begin:
    a = 0
    raise test
rescue Exception:
    begin:
        if a eq(0):
            raise error 10
    rescue error:
        result = error error_number

    test = 10
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("result").value == 10
    assert w_module.getvalue("test").value == 10


def test_built_in_exception_divison_by_zero():
    prog = """
begin:
    a = 5 div(0)
rescue ZeroDivisionException:
    result = 10
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("result").value == 10
    assert w_module.getvalue("a") is None


def test_built_in_exception_divison_by_zero_for_modulo():
    prog = """
begin:
    a = 5 mod(0)
rescue ZeroDivisionException:
    result = 10
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("result").value == 10
    assert w_module.getvalue("a") is None


def test_built_in_exception_wrong_type():
    prog = """
def f:
    return

begin:
    a = f
    b = 10

    test = b add(a)
rescue WrongTypeException:
    result = 10
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("b").value == 10
    assert isinstance(w_module.getvalue("a"), W_None)
    assert w_module.getvalue("result").value == 10


def test_built_in_exception_wrong_type_reverse_argument_order():
    prog = """
def f:
    return

begin:
    a = f
    b = 10

    test = a add(b)
rescue MethodCallOnNoneException:
    result = 10
"""

    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("b").value == 10
    assert isinstance(w_module.getvalue("a"), W_None)
    assert w_module.getvalue("result").value == 10


def test_built_in_exception_method_missing():
    prog = """
object obj:
    a = 0
    b = 10
    #c = 21
    d = 20

begin:
    obj missing
rescue MethodMissingException:
    result = 123456789
"""
    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("result").value == 123456789


def test_built_in_exception_method_missing_on_global():
    prog = """
begin:
    a = fib(10)
rescue MethodMissingException:
    result = 123456789
"""
    w_module = parse_ast_and_return_w_module(prog)

    assert w_module.getvalue("result").value == 123456789