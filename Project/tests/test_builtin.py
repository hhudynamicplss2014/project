import py

from test_helper import *

from simpleparser import parse
from objmodel import W_NormalObject
from interpreter import Interpreter

def test_builtin_simple():
    builtincode = """
x = 1
object None:
    1
def pass:
    None
"""
    ast = """
tx = x
object a:
    pass
ax = a x
"""

    w_module = parse_ast_and_return_w_module(ast, builtincode)
    builtins = w_module.getparents()[0]

    assert builtins.getvalue('x').value == 1
    assert w_module.getvalue("ax").value == 1
    assert w_module.getvalue("tx").value == 1

def test_inttrait():
    builtincode = """
object inttrait:
    x = 1
    def maybe_fortytwo:
        if self:
            42
        else:
            x
"""

    ast = """
x = 5 x # this returns 1, because it looks in the inttrait defined above
m0 = 0 maybe_fortytwo
m1 = x maybe_fortytwo
inttrait x = 2
m2 = 0 maybe_fortytwo
tr = inttrait
"""

    w_module = parse_ast_and_return_w_module(ast, builtincode)
    builtins = w_module.getparents()[0]
    inttrait = builtins.getvalue("inttrait")

    x = w_module.getvalue("x")

    assert w_module.getvalue("tr") is inttrait
    assert inttrait.getparents() == [builtins]
    assert x.value == 1
    assert x.getparents() == [inttrait]
    assert w_module.getvalue("m0").value == 1
    assert w_module.getvalue("m1").value == 42
    assert w_module.getvalue("m2").value == 2

    
def test_builtin_default():
    ast = """
def sumupto(x):
    r = 0
    while x:
        r = r add(x)
        x = x add(-1)
    r
x = sumupto(100)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("x").value == 5050


def test_builtin_boolean():
    ast = """
a = true
b = false
c = not(true)
d = not(false)

a_11 = and(true, true)
a_10 = and(true, false)
a_01 = and(false, true)
a_00 = and(false, false)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("a").value == 1
    assert w_module.getvalue("b").value == 0
    assert w_module.getvalue("c").value == 0
    assert w_module.getvalue("d").value == 1
    assert w_module.getvalue("a_11").value == 1
    assert w_module.getvalue("a_10").value == 0
    assert w_module.getvalue("a_01").value == 0
    assert w_module.getvalue("a_00").value == 0


def test_builtin_inttrait_methods():
    ast = """
a = 1 add(20)
b = 2 sub(3)
c = 0 mul(340)
d = 0 div(32)
e = 20 mod(3)
"""

    w_module = parse_ast_and_return_w_module(ast)

    w_module.getvalue("a").value == 21
    w_module.getvalue("b").value == -1
    w_module.getvalue("c").value == 0
    w_module.getvalue("d").value == 0
    w_module.getvalue("e").value == 2

