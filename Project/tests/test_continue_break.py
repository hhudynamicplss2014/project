from test_helper import *

def test_simple_continue():
    ast = """
result = 10
outcome = 0
while result:
    result = result sub(1)

    if result leq(5):
        continue

    outcome = outcome add(1)
"""
    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 0
    assert w_module.getvalue("outcome").value == 4


def test_nested_continue():
    ast = """
result = 10
outcome = 0
second = 0
while result:
    result = result sub(1)

    if result leq(5):
        continue

    temp = 10
    while temp:
        temp = temp sub(1)

        if temp leq(5):
            continue

        second = second add(1)

    outcome = outcome add(1)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 0
    assert w_module.getvalue("second").value == 16
    assert w_module.getvalue("outcome").value == 4


def test_simple_break():
    ast = """
result = 10
while result:
    if result eq(5):
        break

    result = result sub(1)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 5


def test_nested_break():
    ast = """
result = 10
second = 20
while result:
    if result eq(5):
        break

    while second:
        second = second sub(1)
        break

    result = result sub(1)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 5
    assert w_module.getvalue("second").value == 15


def test_break_makes_last_part_of_loop_useless():
    ast = """
result = 1
while result:
    break
    result = 20
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 1


def test_continue_makes_last_part_of_loop_useless():
    ast = """
result = 10
second = 0
while result:
    result = result sub(1)
    continue
    second = second add(1)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 0
    assert w_module.getvalue("second").value == 0


def test_continue_deep_nested_and_direct_parent_is_not_while():
    ast = """
result = 10

while 1:
    while 1:
        while result:
            result = result sub(1)
            if true:
                continue
            result = result add(1)
        break
    break
"""
    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 0


def test_double_nested():
    ast = """
while 1:
    while 1:
        break
    break
test = 0
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("test").value == 0


def test_deep_nested():
    ast = """
object obj:
    def f(start, limit):
        while start:
            while limit:
                if limit eq(10):
                    break
                else:
                    limit = limit sub(1)

            start = start sub(5)
            break

        start

result = obj f(5, 15)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 0


def test_simple_nested():
    ast = """
start = 10
result = 10

while start:
    while result:
        if result eq(5):
            break
        result = result sub(1)
    result = 10
    if start eq(5):
        break
    start = start sub(1)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 10
    assert w_module.getvalue("start").value == 5

def test_skip_break_nested():
    ast = """
counter = 0
while 1:
    while 1:
        while 1:
            counter = counter add(1)

            if counter leq(150):
                continue

            if counter gt(100):
                break
        break
    break
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("counter").value == 151