from test_helper import *

def test_method_simple():
    ast = """
object a:
    x = 11
    def f:
        self x
object b:
    __parent__ = a
    x = 22
af = a f # a is the receiver, therefore self is a in the method
bf = b f # b is the receiver, therefore self is b in the method

"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("af").value == 11
    assert w_module.getvalue("bf").value == 22


def test_method_explicit_self():
    ast = """
a = 10
def f(a):
    self a = a

f(20)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("a").value == 20


def test_inner_value_shadows_global_value():
    ast = """
a = 10
def f(a):
    a = a mul(10)

f(50)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("a").value == 10


def test_assign_shadowed_to_self():
    ast = """
a = 10
def f(a):
    self a = a

f(20)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("a").value == 20


def test_method_complex():
    ast = """
k = 10
object a:
    x = 11
    y = 22
    def f(a, b):
        if a:
            if b:
                a
            else:
                k
        else:
            if b:
                self x
            else:
                self y
object b:
    __parent__ = a
    y = 55
af11 = a f(1, 1)
af10 = a f(1, 0)
af01 = a f(0, 1)
af00 = a f(0, 0)
k = 908
bf11 = b f(1, 1)
bf10 = b f(1, 0)
bf01 = b f(0, 1)
bf00 = b f(0, 0)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("af11").value == 1
    assert w_module.getvalue("af10").value == 10
    assert w_module.getvalue("af01").value == 11
    assert w_module.getvalue("af00").value == 22
    assert w_module.getvalue("bf11").value == 1
    assert w_module.getvalue("bf10").value == 908
    assert w_module.getvalue("bf01").value == 11
    assert w_module.getvalue("bf00").value == 55