from objmodel import *


def test_normal_init():
    w1 = W_NormalObject()
    w2 = W_Integer(0)
    w3 = W_Method('reference')

    assert isinstance(w1, W_NormalObject) is True
    assert isinstance(w2, W_Integer) is True
    assert isinstance(w3, W_Method) is True


def test_normal_setvalue():
    w1 = W_NormalObject()
    assert w1.getvalue('whatever') is None

    w1.setvalue('whatever', W_Integer(20))
    assert w1.getvalue('whatever').value == 20

    w1.setvalue('whatever', False)
    assert w1.getvalue('whatever') is False

    w2 = W_NormalObject()
    assert w2.getvalue('whatever') is None

    w2.setvalue('whatever', 'string')
    assert w2.getvalue('whatever') == 'string'

    w3 = W_Method('ref')
    w3.setvalue('test', 0)
    assert w3.getvalue('test') == 0

    w4 = W_Integer(0)
    w4.setvalue('something', 20)
    assert w4.getvalue('something') is None
    assert w4.value == 20


def test_getvalue_simple():
    w1 = W_NormalObject()
    w1.setvalue("whatever", "something")
    assert w1.getvalue('whatever') == 'something'

    w2 = W_Method('ref')
    w2.setvalue("big", "enough")
    assert w2.getvalue("big") == "enough"

    w3 = W_Integer(10)
    w3.getvalue("anything") is None


def test_getvalue_inheritance():
    w1 = W_NormalObject()
    w1.setvalue("something", 0)
    w1.setvalue("further", 0)

    w2 = W_Method('ref', w1)
    w2.setvalue("anything", 4)

    assert w2.getvalue("anything") == 4
    assert w1.getvalue("something") == 0
    assert w2.getvalue("something") == 0

    assert w2.getvalue("nothing") is None
    assert w1.getvalue("nothing") is None

    w2.setvalue("something", -1)

    assert w2.getvalue("something") == -1
    assert w1.getvalue("something") == 0

    w3 = W_NormalObject(w2)
    w3.setvalue("timbob", 42)
    assert w3.getvalue("timbob") == 42
    assert w3.getvalue("anything") == 4

    assert w2.getvalue("timbob") is None

    w4 = W_NormalObject(w2)
    assert w4.getvalue("further") == 0

    a = W_NormalObject(None)
    a.setvalue("x", 1)
    a.setvalue("y", 2)
    a.setvalue("z", 10)

    b = W_NormalObject(a)
    b.setvalue("y", 5)
    b.setvalue("z", 5)

    assert b.getvalue("x") == 1


def test_istrue():
    w1 = W_NormalObject()
    assert w1.istrue() is True

    w2 = W_Method('ref')
    assert w2.istrue() is True

    w3 = W_Integer(0)
    assert w3.istrue() is False
    w3.value = 10
    assert w3.istrue() is True


def test_clone():
    w1 = W_NormalObject()
    w1.setvalue('abc', W_Integer(6))
    w2 = w1.clone()

    assert isinstance(w2, W_NormalObject) is True
    assert w2.getvalue('abc').value == 6

    w2.setvalue('abc', W_Integer(99))

    assert w2.getvalue('abc').value == 99
    assert w1.getvalue('abc').value == 6

    assert w1.getvalue("__parent__") is None
    assert w2.getvalue("__parent__") is w1

    w1 = W_Integer(0)
    w2 = w1.clone()

    assert w1.value == 0
    assert w2.value == 0

    w1.value = 5
    w2.value = 10

    assert w1.value == 5
    assert w2.value == 10

    w5 = W_Method('ref', None)
    w6 = w5.clone()

    assert w6.reference == 'ref'


def test_getparents():
    w1 = W_NormalObject()
    assert w1.getparents() == [None]

    w2 = W_Method("ref", w1)
    assert w2.getparents() == [w1]
    assert w1.getparents() == [None]

    w3 = W_Integer(0, [("w2", w2), ("w1", w1)])
    assert w3.getparents() == [w2, w1]
    assert w2.getparents() == [w1]
    assert w1.getparents() == [None]


def test_get_mro():
    w1 = W_NormalObject()
    w2 = W_Integer(0, w1)
    w3 = W_Method('ref', [("w2", w2), ("w1", w1)])
    w4 = W_NormalObject([("w3", w3), ("w1", w1)])

    assert w4.get_mro() == [w4, w3, w2, w1]
    assert w3.get_mro() == [w3, w2, w1]
    assert w2.get_mro() == [w2, w1]
    assert w1.get_mro() == [w1]


def test_instanciated_with_name():
    w1 = W_NormalObject()
    assert w1.name == id(w1)

    w2 = W_NormalObject(w1, "w2")
    assert w1.name == id(w1)
    assert w2.name == "w2"

    w3 = W_Method('ref', None, "w3")
    assert w1.name == id(w1)
    assert w2.name == "w2"
    assert w3.name == "w3"

    w4 = W_Integer(0, [("w2", w2), ("w3", w3)], "w4")
    assert w1.name == id(w1)
    assert w2.name == "w2"
    assert w3.name == "w3"
    assert w4.name == "w4"


def test_set_new_parent():
    w1 = W_Integer(0)

    w2 = W_NormalObject()
    assert w2.getvalue("__parent__") is None

    w2.setvalue("__parent__", w1)
    assert w2.getvalue("__parent__") is w1

    w4 = W_Method('ref')
    w4.setvalue("x", 0)

    w3 = W_NormalObject(w2)
    assert w3.getvalue("__parent__") is w2

    w3.setvalue("__parent__", w1)
    w3.getvalue("__parent__") is w1


def test_set_new_parent_multiple():
    w1 = W_NormalObject()
    w2 = W_Method('ref')
    w3 = W_Integer(0)

    w4 = W_NormalObject([("w1", w1), ("w2", w2)])
    assert w4.getparents() == [w1, w2]

    w4.setvalue("w1", w3)

    assert w4.getparents() == [w3, w2]


def test_w_none_clone():
    w1 = W_None()
    w2 = w1.clone()

    assert isinstance(w2, W_None) == True


def test_set_parent_exception():
    parent = W_Integer(10)

    w1 = W_Exception(None, 'Name', 20)
    assert w1.getparents() == [None]

    w1.setParent(parent)
    assert w1.getvalue('__parent__') == parent

    assert w1.getparents() == [parent]


def test_pass_integer_to_exception_constructor():
    w1 = W_Exception(None, 'Name', 20)

    assert isinstance(w1.getvalue('error_number'), W_Integer) == True
    assert w1.getvalue('error_number').value == 20

    w2 = W_Exception(None, 'Name', None)

    assert isinstance(w2.getvalue('error_number'), W_None) == True


def test_return_value_from_object():
    w1 = W_ReturnObject('return value')
    assert w1.return_value == 'return value'

    w2 = W_ReturnObject()
    assert w2.return_value is None