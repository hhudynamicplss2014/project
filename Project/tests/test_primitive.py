from test_helper import *

"""
    Please note that test cases, where primitives cause exceptions on purpose are in test_exceptions.py
"""

def test_primitive():
    ast = """
k = 10 $int_add(31)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("k").value == 41


def test_loop():
    ast = """
def f(x):
    r = 0
    while x:
        r = r $int_add(x)
        x = x $int_add(-1)
    r
y = f(100)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("y").value == 5050


def test_if_with_primitive():
    ast = """
def f(cmp):
    if cmp eq(10):
        1
    else:
        2
first = f(10)
second = f(20)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("first").value == 1
    assert w_module.getvalue("second").value == 2


def test_while_with_primitive():
    ast = """
result = 10
temp = 0
while result geq(0):
    result = result sub(1)
    temp = temp add(1)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("result").value == -1
    assert w_module.getvalue("temp").value == 11


def test_top_level_syntax():
    ast = """
result1 = 0
result1 = result1 add(1)

result2 = 0
result2 = result2 sub(10)

result3 = result2 mul(result1)
result4 = result2 div(result3)

eq = result3 eq(result3)
neq = result3 neq(result3)
geq = result3 geq(result1)
leq = result2 leq(result4)
lt = result2 lt(result2)
gt = result3 gt(result4)
    """

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("result1").value == 1
    assert w_module.getvalue("result2").value == -10
    assert w_module.getvalue("result3").value == -10
    assert w_module.getvalue("result4").value == 1

    assert w_module.getvalue("eq").value == 1
    assert w_module.getvalue("neq").value == 0
    assert w_module.getvalue("geq").value == 0
    assert w_module.getvalue("leq").value == 1
    assert w_module.getvalue("lt").value == 0
    assert w_module.getvalue("gt").value == 0