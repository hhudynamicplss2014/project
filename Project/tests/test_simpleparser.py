import py

from simpleparser import Parser, AbstractParseError, SemanticParseError
from simplelexer import lex
from simpleast import *

def raisesparserror(source, what):
    p = Parser(source)
    excinfo = py.test.raises(AbstractParseError, getattr(p, what))
    print excinfo.value.nice_error_message()


def test_expression():
    print "i"

    ast = Parser("a b c").expression()
    ast1 = MethodCall(MethodCall(MethodCall(ImplicitSelf(), "a"), "b"), "c")
    assert ast == ast1
    ast = Parser("a f(1, a b c, 3,)").expression()
    assert ast == MethodCall(MethodCall(ImplicitSelf(), "a"), "f", [
                      IntLiteral(1), ast1, IntLiteral(3)])

def test_expression2():
    ast = Parser("$a $b $c").expression()
    ast1 = PrimitiveMethodCall(PrimitiveMethodCall(
        PrimitiveMethodCall(ImplicitSelf(), "$a"), "$b"), "$c")
    assert ast == ast1
    ast = Parser("$a $f(1, $a $b $c, 3,)").expression()
    assert ast == PrimitiveMethodCall(
            PrimitiveMethodCall(ImplicitSelf(), "$a"), "$f", [
                      IntLiteral(1), ast1, IntLiteral(3)])


def test_simplestatement():
    ast = Parser("a\n").statement()
    ast1 = ExprStatement(MethodCall(ImplicitSelf(), "a"))
    assert ast == ast1
    ast = Parser("a = 4 b\n").statement()
    ast1 = Assignment(ImplicitSelf(), "a", MethodCall(IntLiteral(4), "b"))
    assert ast == ast1
    ast = raisesparserror("a(1) = 4 b\n", "statement")
    ast = raisesparserror("1 = 4 b\n", "statement")

def test_error():
    ast = raisesparserror("a add 2\n", "statement")



def test_if():
    ast = Parser("""if a and(b):
    a b
""").statement()
    ast1 = IfStatement(MethodCall(MethodCall(ImplicitSelf(), "a"), "and",
                                  [MethodCall(ImplicitSelf(), "b")]),
                       Program([ExprStatement(
                           MethodCall(MethodCall(ImplicitSelf(), "a"),
                                "b"))]))
    assert ast1 == ast

    ast = Parser("""if a and(b):
    a b
else:
    b
""").statement()
    ast1 = IfStatement(MethodCall(MethodCall(ImplicitSelf(), "a"), "and",
                                  [MethodCall(ImplicitSelf(), "b")]),
                       Program([ExprStatement(
                           MethodCall(MethodCall(ImplicitSelf(), "a"),
                                "b"))]),
                       Program([ExprStatement(
                           MethodCall(ImplicitSelf(), "b"))]))
    assert ast1 == ast

def test_while():
    ast = Parser("""
while i:
    i = i sub(1)
""").statement()
    ast1 = WhileStatement(MethodCall(ImplicitSelf(), "i"),
                          Program([Assignment(ImplicitSelf(), "i",
                                      MethodCall(MethodCall(ImplicitSelf(), "i"),
                                                 "sub",
                                                 [IntLiteral(1)]))]))
    assert ast1 == ast

def test_while_with_break():
    ast = Parser("""
while i:
    break
""").statement()
    ast1 = WhileStatement(MethodCall(ImplicitSelf(), "i", []),
                          Program([BreakStatement()]))

    assert ast1 == ast

def test_while_with_continue():
    ast = Parser("""
while i:
    continue
""").statement()
    ast1 = WhileStatement(MethodCall(ImplicitSelf(), "i", []),
                          Program([ContinueStatement()]))

    assert ast1 == ast

def test_while_with_deep_break():
    ast = Parser("""
while i:
    if a:
        if b:
            a = 0
        else:
            break

        continue
    else:
        break
""").statement()

    print ast

    ast1 = WhileStatement(MethodCall(ImplicitSelf(), 'i', []),
                          Program([IfStatement(MethodCall(ImplicitSelf(), 'a', []),
                                               Program([IfStatement(MethodCall(ImplicitSelf(), 'b', []),
                                                                    Program([Assignment(ImplicitSelf(), 'a', IntLiteral(0))]),
                                                                    Program([BreakStatement()])), ContinueStatement()]),
                                               Program([BreakStatement()]))]))

    assert ast == ast1

def test_while_with_break_and_continue():
    ast = Parser("""
while i:
    if i:
        continue
    else:
        break
""").statement()
    ast1 = WhileStatement(MethodCall(ImplicitSelf(), "i", []),
                          Program([IfStatement(MethodCall(ImplicitSelf(), "i", []),
                                               Program([ContinueStatement()]),
                                               Program([BreakStatement()]))]))

    assert ast1 == ast

def test_break_without_while():
    ast = """
break
"""
    raisesparserror(ast, 'statement')

def test_continue_without_while():
    ast = """
continue
"""
    raisesparserror(ast, 'statement')


def test_nested_break_multiple():
    ast = Parser("""
while 1:
    while 1:
        a = 0
    break
""").statement()
    ast1 = WhileStatement(IntLiteral(1), Program(
            [WhileStatement(IntLiteral(1), Program([Assignment(ImplicitSelf(), 'a', IntLiteral(0))])), BreakStatement()]))
    assert ast == ast1


def test_nested_break_multiple_alternative():
    ast = Parser("""
if 1:
    while 1:
        a = 0

while 1:
    break
""").statement()
    ast1 = IfStatement(IntLiteral(1), Program(
            [WhileStatement(IntLiteral(1), Program([Assignment(ImplicitSelf(), 'a', IntLiteral(0))]))]), None)
    assert ast == ast1

def test_begin_simple():
    ast = Parser("""
begin:
    a = 0
rescue error:
    a = 1
""").statement()
    ast1 = BeginStatement(Program([Assignment(ImplicitSelf(), 'a', IntLiteral(0))]),
                          [RescueStatement('error', Program([Assignment(ImplicitSelf(), 'a', IntLiteral(1))]))])
    assert ast == ast1

def test_begin_multiple_rescue():
    ast = Parser("""
begin:
    a = 0
rescue error1:
    a = 1
rescue error2:
    a = 2
""").statement()
    ast1 = BeginStatement(Program([Assignment(ImplicitSelf(), 'a', IntLiteral(0))]),
                          [
                            RescueStatement('error1', Program([Assignment(ImplicitSelf(), 'a', IntLiteral(1))])),
                            RescueStatement('error2', Program([Assignment(ImplicitSelf(), 'a', IntLiteral(2))]))
                          ])
    assert ast == ast1

def test_begin_without_rescue():
    ast = """
begin:
    a = 0
"""
    raisesparserror(ast, 'statement')

def test_begin_rescue_only():
    ast = """
rescue first:
    a = 0
"""
    raisesparserror(ast, 'statement')

def test_begin_rescue_without_name():
    ast = """
begin:
    a = 0
rescue:
    a = 1
"""
    raisesparserror(ast, 'statement')

def test_object():
    ast = Parser("""
object a:
    i = 1
    if i:
        j = 2
""").statement()
    ast1 = ObjectDefinition("a", Program([
        Assignment(ImplicitSelf(), "i", IntLiteral(1)),
        IfStatement(MethodCall(ImplicitSelf(), "i"), Program([
            Assignment(ImplicitSelf(), "j", IntLiteral(2)),
            ]))
        ]))
    assert ast1 == ast

    ast = Parser("""
object a(parent=1):
    i = 1
    if i:
        j = 2
""").statement()
    ast1 = ObjectDefinition("a", Program([
        Assignment(ImplicitSelf(), "i", IntLiteral(1)),
        IfStatement(MethodCall(ImplicitSelf(), "i"), Program([
            Assignment(ImplicitSelf(), "j", IntLiteral(2)),
            ]))
        ]),
        ["parent"],
        [IntLiteral(1)])
    assert ast1 == ast


def test_def():
    ast = Parser("""
def f(x, y, z):
    i = 1
    if i:
        j = 2
""").program()
    ast1 = Program([FunctionDefinition("f", ["x", "y", "z"], Program([
        Assignment(ImplicitSelf(), "i", IntLiteral(1)),
        IfStatement(MethodCall(ImplicitSelf(), "i"), Program([
            Assignment(ImplicitSelf(), "j", IntLiteral(2)),
            ]))
        ]))])
    assert ast1 == ast

def test_def_with_simple_return():
    ast = Parser("""
def f(x):
    return
""").program()
    ast1 = Program([FunctionDefinition("f", ["x"], Program([
        ReturnStatement(None)
        ]))])
    assert ast1 == ast

def test_def_with_return_value():
    ast = Parser("""
def f(x):
    return x
""").program()
    ast1 = Program([FunctionDefinition("f", ["x"], Program([
        ReturnStatement(MethodCall(ImplicitSelf(), "x"))])
    )])
    assert ast1 == ast


def test_return_with_function_call_value():
    ast = Parser("""
def f(a):
    return a
def g(a):
    return f(a)

a = g(1)
""").program()
    ast1 = Program(
        [FunctionDefinition('f', ['a'], Program([ReturnStatement(MethodCall(ImplicitSelf(), 'a', []))])),
         FunctionDefinition('g', ['a'], Program([ReturnStatement(MethodCall(ImplicitSelf(), 'f', [MethodCall(ImplicitSelf(), 'a', [])]))])),
         Assignment(ImplicitSelf(), 'a', MethodCall(ImplicitSelf(), 'g', [IntLiteral(1)]))])

    assert ast1 == ast


def test_return_with_no_basic_expression_value():
    ast = """
def f(a):
    return a mul(2)
"""
    raisesparserror(ast, 'returnstatement')


def test_def_with_literal_return_value():
    ast = Parser("""
def const:
    return 0
""").program()
    ast1 = Program([FunctionDefinition("const", [], Program([
        ReturnStatement(IntLiteral(0))
    ]))])
    assert ast == ast1

def test_return_without_def():
    ast = """
return 0
"""
    raisesparserror(ast, 'statement')

    ast = """
return
def f:
    a = 0
"""
    raisesparserror(ast, 'statement')


def test_simple_raise():
    ast = Parser("""
raise Name
""").statement()
    ast1 = RaiseStatement('Name', None)
    assert ast == ast1


def test_multiple_raise():
    ast = Parser("""
if a:
    raise first
else:
    raise second
raise third
""").program()
    ast1 = Program([IfStatement(MethodCall(ImplicitSelf(), 'a', []), Program([RaiseStatement('first', None)]),
                                Program([RaiseStatement('second', None)])), RaiseStatement('third', None)])
    assert ast == ast1

def test_raise_with_error_number():
    ast = Parser("""
raise first 1024
""").statement()
    ast1 = RaiseStatement('first', IntLiteral(1024))
    assert ast == ast1


def test_raise_mixed():
    ast = Parser("""
if a:
    raise first -1
else:
    raise second 0
raise third
""").program()
    ast1 = Program([IfStatement(MethodCall(ImplicitSelf(), 'a', []), Program([RaiseStatement('first', IntLiteral(-1))]),
                                Program([RaiseStatement('second', IntLiteral(0))])), RaiseStatement('third', None)])
    assert ast == ast1

def test_raise_without_name():
    ast = """
raise
"""
    raisesparserror(ast, 'raisestatement')


def test_raise_with_more_parameters():
    ast = """
raise exc 0 20
"""
    raisesparserror(ast, 'raisestatement')


def test_raise_with_wrong_type_for_number():
    ast = """
raise exc number
"""
    raisesparserror(ast, 'raisestatement')


def test_raise_with_wrong_type_for_name():
    ast = """
raise 20 0
"""
    raisesparserror(ast, 'raisestatement')


def test_no_statements_in_block():
    ast = """
if a:
else:
    a = 20
"""
    raisesparserror(ast, 'statement')


def test_level_of_indentation():
    ast = Parser("""
while 1:
    if a:
        while 1:
            if b:
                c = 0
            else:
                d
    else:
        k = 10
""")
    # while to if
    assert ast.level_of_indentation(1, 6) == 1
    # while to while
    assert ast.level_of_indentation(1, 11) == 2
    # while to second if
    assert ast.level_of_indentation(1, 16) == 3
    # while to c assignment
    assert ast.level_of_indentation(1, 21) == 4
    # while to else
    assert ast.level_of_indentation(1, 26) == 3
    # while to d
    assert ast.level_of_indentation(1, 30) == 4
    # while to second else
    assert ast.level_of_indentation(1, 35) == 1
    # while to k assignment
    assert ast.level_of_indentation(1, 40) == 2
    #second while to if
    assert ast.level_of_indentation(11, 16) == 1
    #second while to d
    assert ast.level_of_indentation(11, 30)


def test_position_of_last_command():
    ast = Parser("""
while 1:
    if a:
        while 1:
            if b:
                c = 0
            else:
                d
    else:
        k = 10
""")
    last = len(ast.tokens)-1

    assert ast.position_of_last_command(last, 'else') == 35
    assert ast.position_of_last_command(34, 'else') == 26

    assert ast.position_of_last_command(last, 'while') == 11
    assert ast.position_of_last_command(10, 'while') == 1

    assert ast.position_of_last_command(25, 'else') == -1
    assert ast.position_of_last_command(last+2, 'while') == -1


def test_is_within_indent_of_command():
    ast = Parser("""
while 1:
    if a:
        while 1:
            if b:
                c = 0
            else:
                d
    else:
        k = 10
""")
    #is first if within while?
    assert ast.is_within_indent_of_command(6, 'while') == True
    #is second if within while?
    assert ast.is_within_indent_of_command(16, 'while') == True
    #is second else within a while loop?
    assert ast.is_within_indent_of_command(35, 'while') == True
    #is second while within an else block?
    assert ast.is_within_indent_of_command(11, 'else') == False


def test_is_correct_token():
    ast = Parser("""
while 1:
    if a:
        while 1:
            if b:
                c = 0
            else:
                d
    else:
        k = 10
""")
    assert ast.is_correct_token(1, 'Name', 'while') == True
    assert ast.is_correct_token(11, 'Name', 'while') == True
    assert ast.is_correct_token(35, 'Name', 'if') == False

    #you may also check non commands
    assert ast.is_correct_token(36, 'Special', ':') == True


def test_do_indentation_check_on_current_token():
    ast = Parser("""
while 1:
    if a:
        while 1:
            if b:
                c = 0
            else:
                d
    else:
        k = 10
""")
    ast.i = 11
    ast.last_i = 10

    ast.do_indedation_check_on_current_token('while', 'while')

    with py.test.raises(AbstractParseError) as ex:
        ast.do_indedation_check_on_current_token('while', 'else')
    assert isinstance(ex.value, SemanticParseError) == True

    ast.i = 35
    ast.last_i = 34

    #ignore if token is not correct
    ast.do_indedation_check_on_current_token('if', 'while')
    ast.do_indedation_check_on_current_token('if', 'else')

    #now with correct token

    ast.do_indedation_check_on_current_token('else', 'while')

    with py.test.raises(AbstractParseError) as ex:
        ast.do_indedation_check_on_current_token('else', 'if')
    assert isinstance(ex.value, SemanticParseError) == True