__author__ = 'das heck'

from interpreter import *
from simpleparser import parse

def parse_ast_and_return_w_module(ast, custom_builtin = None):
    interpreter = Interpreter(custom_builtin)
    w_module = interpreter.make_module()
    interpreter.eval(parse(ast), w_module)

    return w_module
