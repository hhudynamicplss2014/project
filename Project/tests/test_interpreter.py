from test_helper import *

def test_simple():
    ast = """
k = 10
j = 11
i = 12
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("i").value == 12
    assert w_module.getvalue("j").value == 11
    assert w_module.getvalue("k").value == 10


def test_if():
    ast = """
k = 10
if k:
    j = 11
else:
    i = 12
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("i") is None
    assert w_module.getvalue("j").value == 11
    assert w_module.getvalue("k").value == 10


def test_def():
    ast = """
def f(x, y):
    if x:
        x
    else:
        y
i = f(6, 3)
j = f(0, 9)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("i").value == 6
    assert w_module.getvalue("j").value == 9


def test_object():
    ast = """
object x:
    i = 4
    j = i
    object z:
        i = 5
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("x").getvalue("i").value == 4
    assert w_module.getvalue("x").getvalue("j").value == 4
    assert w_module.getvalue("x").getvalue("z").getvalue("i").value == 5


def test_obscure_recursion():
    ast = """
object a:
    def f(a):
        if a:
            x = 5
            a f(0)
        else:
            x = 7
        x
i = a f(a)
"""

    w_module = parse_ast_and_return_w_module(ast)
    assert w_module.getvalue("i").value == 5


def test_new_feature_set():
    """
        Here we use certain new features of our project, namely exception handling, return values and break. We define a
        method f, which takes two arguments. These arguments affect the execution of the function body. If b equals 0,
        we throw a bad_argument exception immediately. If not, we execute code, which returns -1 if b >= a and 1 otherwise.
        This code is a little inconvenient for clarification and usage of the return statement, which also breaks loops.

        The method f gets called in a begin block, which handles all exceptions. First we call f with a > b, which will
        return -1, which then raises a bad_return_value exception. This will set the flag to its error_number, which we
        set upon raising as optional argument. Therefore after execution the flag will have the value 1024.
    """
    ast = """
def f(a,b):
    if b eq(0):
        raise bad_argument
    else:
        while a:
            if b eq(0):
                return -1
            else:
                a = a sub(1)
                b = b sub(1)

            if a eq(0):
                return 1

result = 0
flag = 0

begin:
    temp = f(10, 8)

    if temp eq(-1):
        raise bad_return_value 1024

    if temp eq(1):
        result = 1

rescue bad_argument:
    flag = 1
rescue bad_return_value:
    flag = bad_return_value error_number
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("result").value == 0
    assert w_module.getvalue("flag").value == 1024


def test_new_feature_set():
    """
        This example shows the return statement and advanced exception handling. We start by running into a endless
        loop, which will only be exited by break. We call a function f twice, which returns depending on the
        argument either W_None or a W_Integer. When we try to use both results in an arithmetic operation such as
        mul, we provoce a builtin exception called 'WrongTypeException', which is called when the arguments do not
        match as intended. We expect both receiver and parameter to be a W_Integer type, when calling mul. By catching
        this exception we set a flag result to 0 (look at primitive.py for further information about error numbers of
        builtin exceptions). This will cause another exception, when using this as parameter for a division.
        Bottom line is, that the break will never be called, since the line above searches for the next rescue block,
        which is the outer 'Exception' (catch all) block, which sets the exc to -1, which is the known error number
        for ZeroDivisionException.
    """

    ast = """
def f(a):
    if a eq(0):
        return

    retval = a mul(2)
    return retval

begin:
    while 1:
        temp = f(1)
        second = f(0)

        begin:
            result = temp mul(second)
        rescue WrongTypeException:
            result = WrongTypeException error_number

        final = temp div(result)
        break
rescue Exception:
    exc = Exception error_number
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("exc").value == -1