from test_helper import *

def test_return_with_integer_value():
    ast = """
def const:
    return 10

test = const
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("test").value == 10

def test_return_with_simple_value():
    ast = """
def f(a,b):
    result = a add(b)
    return result

test = f(5,5)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("test").value == 10

def test_return_with_complex_value():
    ast = """
def const(a,b):
    condition = a eq(3)

    if condition:
        return b
    else:
        return a

def f(a,b):
    return const(a,b)

first = f(3, 5)
second = f(10, 5)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("first").value == 5
    assert w_module.getvalue("second").value == 10

def test_return_without_value():
    ast = """
def f:
    result = 1
    while result:
        return

a = f
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert isinstance(w_module.getvalue("a"), W_None)


def test_return_without_value_and_self():
    ast = """
result = 10
def f(limit):
    while self result:
        if self result eq(limit):
            return

        self result = self result sub(1)

t = f(5)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert isinstance(w_module.getvalue("t"), W_None)
    assert w_module.getvalue("result").value == 5


def test_return_must_also_break_from_loops():
    ast = """
def f:
    result = 1
    while result:
        return 1024

a = f
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("a").value == 1024


def test_return_must_also_break_from_nested_loops():
    ast = """
def f(a):
    while 1:
        while 1:
            while 1:
                while 1:
                    if a:
                        return a
                    else:
                        return

first = f(10)
second = f(0)
"""
    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("first").value == 10
    assert isinstance(w_module.getvalue("second"), W_None)


def test_after_return_nothing_gets_executed():
    ast = """
result = 10
def f(a):
    return a
    result = 42

test = f(50)
"""

    w_module = parse_ast_and_return_w_module(ast)

    assert w_module.getvalue("test").value == 50
    assert w_module.getvalue("result").value == 10