import py
from simpleparser import parse
from objmodel import *
import primitive

defaultbuiltin = """
def true:
    1
def false:
    0
def not(value):
    if value eq(true):
        false
    else:
        true
def and(first, second):
    if first eq(true):
        if second eq(true):
            return true
    false
object inttrait:
    def add(a):
        self $int_add(a)
    def sub(a):
        self $int_sub(a)
    def mul(a):
        self $int_mul(a)
    def div(a):
        self $int_div(a)
    def mod(a):
        self $int_mod(a)
    def eq(a):
        self $int_eq(a)
    def neq(a):
        self $int_neq(a)
    def lt(a):
        self $int_lt(a)
    def leq(a):
        self $int_le(a)
    def gt(a):
        self $int_gt(a)
    def geq(a):
        self $int_ge(a)
"""

class Interpreter(object):
    def __init__(self, builtin = None):
        self.primitives = primitive.registry

        if builtin is None:
            self.builtin = defaultbuiltin
        else:
            self.builtin = builtin

    def make_module(self):
        return W_NormalObject(self.eval(parse(self.builtin), W_NormalObject(None, "builtin")))

    def eval(self, ast, w_context):
        method = getattr(self, "eval_" + ast.__class__.__name__)
        return method(ast, w_context)

    def eval_BeginStatement(self, ast, w_context):
        """
            Execute the block and store the result. Test if the result is an exceptin. If so
            execute the corresponding rescue block or the catch all block if present. Return the
            context otherwise, which might be an unhandled exception.
        """

        context = self.eval(ast.block, w_context)

        if isinstance(context, W_Exception):
            for rescue in ast.rescue_asts:
                if rescue.name == context.name or rescue.name == 'Exception':
                    context.setParent(w_context)
                    w_context.setvalue(rescue.name, context)

                    self.eval(rescue, w_context)
                    return w_context

        return context

    def eval_RaiseStatement(self, ast, w_context):
        """
            Check if the error number is None, which is legitimite. If not evaluate the code of the error number
            and store it in the variable. Return an instance of W_Exception.
        """

        if ast.error_number is not None:
            error_number = self.eval(ast.error_number, w_context)
        else:
            error_number = W_None()

        return W_Exception(w_context, ast.name, error_number)
    
    def eval_RescueStatement(self, ast, w_context):
        """
            Execute and return the block.
        """

        return self.eval(ast.block, w_context)

    def eval_BreakStatement(self, ast, w_context):
        """
            Return an instance of W_BreakObject.
        """

        return W_BreakObject(w_context)

    def eval_ContinueStatement(self, ast, w_context):
        """
            Return an instance of W_ContinueObject.
        """

        return W_ContinueObject(w_context)

    def eval_ReturnStatement(self, ast, w_context):
        """
            Check if the return value is none. If not evaluate and store it in a variable. Return an
            instance of W_ReturnObject.
        """

        return_value = W_None()

        if not ast.return_value is None:
            return_value = self.eval(ast.return_value, w_context)

        return W_ReturnObject(return_value, w_context)

    def eval_IntLiteral(self, ast, w_context):
        return W_Integer(int(ast.value), w_context)

    def eval_MethodCall(self, ast, w_context):
        #if ast.receiver is None:
        #    import pdb
        #    pdb.set_trace()

        receiver = self.eval(ast.receiver, w_context)



        if isinstance(receiver, W_None):
            return W_Exception(w_context, 'MethodCallOnNoneException', 0)

        if receiver.getvalue(ast.methodname) is None:
            return W_Exception(w_context, 'MethodMissingException', -1)

        method_object = receiver.getvalue(ast.methodname).clone()

        if isinstance(method_object, W_Method):
            method_object.setvalue("self", receiver)
            method_ast = method_object.reference

            for i in range(len(ast.arguments)):
                arg = self.eval(ast.arguments[i], w_context)
                method_object.setvalue(method_object.reference.arguments[i], arg)

            temp = self.eval(method_ast.block, method_object)

            if isinstance(temp, W_ReturnObject):
                return temp.return_value

            return temp
        else:
            return receiver.getvalue(ast.methodname)

    def eval_WhileStatement(self, ast, w_context):
        if isinstance(w_context, W_ReturnObject):
            return w_context

        condition = self.eval(ast.condition, w_context)

        if condition.istrue():
            context = self.eval(ast.whileblock, w_context)

            if isinstance(context, W_BreakObject):
                return context.getvalue("__parent__")
            elif isinstance(context, W_ContinueObject):
                context = context.getvalue("__parent__")
            elif isinstance(context, W_ReturnObject) or isinstance(context, W_Exception):
                return context

            w_context = self.eval_WhileStatement(ast, context)

            if isinstance(w_context, W_ReturnObject):
                return w_context
            elif isinstance(w_context, W_Exception):
                return w_context

        return w_context

    def eval_PrimitiveMethodCall(self, ast, w_context):
        receiver = self.eval(ast.receiver, w_context)
        method = self.primitives[ast.methodname]
        arg = self.eval(ast.arguments[0], w_context)

        context = method(receiver, arg)
        context.setParent(w_context)

        return context

    def eval_ImplicitSelf(self, ast, w_context):
        return w_context

    def eval_Statement(self, ast, w_context):
        return self.eval(ast.expression, w_context)

    def eval_Assignment(self, ast, w_context):
        lvalue = self.eval(ast.lvalue, w_context)
        expression = self.eval(ast.expression, w_context)

        if isinstance(expression, W_Exception):
            return expression

        lvalue.setvalue(ast.attrname, expression)

        return w_context

    def eval_ExprStatement(self, ast, w_context):
        return self.eval(ast.expression, w_context)

    def eval_IfStatement(self, ast, w_context):
        condition = self.eval(ast.condition, w_context)

        if condition.istrue():
            temp = self.eval(ast.ifblock, w_context)
            return temp
        else:
            if ast.elseblock is not None:
                return self.eval(ast.elseblock, w_context)
            else:
                return w_context

    def eval_FunctionDefinition(self, ast, w_context):
        method = W_Method(ast, w_context)
        w_context.setvalue(ast.name, method)

        return w_context

    def eval_ObjectDefinition(self, ast, w_context):
        parents = []

        for i in range(len(ast.parentnames)):
            parents.append((ast.parentnames[i], self.eval(ast.parentdefinitions[i], w_context)))

        parents.append(("__parent__", w_context))

        temp = self.eval(ast.block, W_NormalObject(parents))
        w_context.setvalue(ast.name, temp)

        return w_context

    def eval_Program(self, ast, w_context):
        for statement in ast.statements:
            context = self.eval(statement, w_context)

            if isinstance(context, W_ReturnContextObject):
                return context

        return context