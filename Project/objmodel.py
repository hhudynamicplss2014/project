from c3computation import *

class W_None(object):
    def __init__(self):
        pass

    def dump(self, indent=0):
        return ("\t" * indent) + "W_None"

    def __str__(self):
        return "W_None"

    def clone(self):
        return W_None()

class W_Base(object):
    def __init__(self, parents = None, name = None):
        if name is None:
            self.name = id(self)
        else:
            self.name = name

        self.parents = None

        if parents is None:
            self.parents = [("__parent__", None)]
        elif isinstance(parents, W_Base):
            self.parents = [("__parent__", parents)]
        elif isinstance(parents, tuple):
            self.parents = [parents]
        elif isinstance(parents, list):
            self.parents = parents

            for item in parents:
                if item[0] != "__parent__":
                    self.setvalue(item[0], item[1])

        # Creates an error if the parents somehow create a bad mro
        self.get_mro()

    def getparents(self):
        return [item[1] for item in self.parents]

    def get_mro(self):
        if self.parents == [("__parent__", None)]:
            return [self]

        return compute_C3_mro(self)

    def setvalue(self, key, value):
        pass

    def getvalue(self, key):
        pass

    def istrue(self):
        pass

    def getname(self):
        return self.name

    def clone(self):
        pass

    def dump(self, indent):
        pass

    def __str__(self):
        pass


class W_Integer(W_Base):
    def __init__(self, value, parents = None, name = None):
        self.value = value
        super(W_Integer, self).__init__(parents, name)

    def setParent(self, value):
        self.parents = [('__parent__', value)]

    def setvalue(self, key, value):
        self.value = value

    def getvalue(self, key):
        if key == "__parent__":
            return self.getparents()[-1]

        mro = self.get_mro()
        mro.pop(0)

        for obj in mro:
            if obj.values.__contains__(key):
                return obj.values[key]

            if obj.values.__contains__('inttrait'):
                return obj.values['inttrait'].getvalue(key)

        return None

    def istrue(self):
        return self.value != 0

    def clone(self):
        return W_Integer(self.value, self)

    def dump(self, indent=0):
        return ("\t" * indent) + "W_Integer(" + str(self.value) + ")"

    def __str__(self):
        if self.getvalue("__parent__") is None:
            parent = "None"
        else:
            parent = str(self.getvalue("__parent__").name)

        return str(self.value) + " (" + parent + ")"


class W_NormalObject(W_Base):
    def __init__(self, parents = None, name = None):
        self.values = {}
        super(W_NormalObject, self).__init__(parents, name)

        self.values["__parent__"] =  self.getparents()[-1]

    def setvalue(self, key, value):
        for i in range(len(self.parents)):
            if key == self.parents[i][0]:
                self.parents[i] = (key, value)

        if key == "__parent__":
            self.parents = []
            self.parents.append(("__parent__", value))

        self.values[key] = value

    def getvalue(self, key):
        if key in self.values:
            return self.values[key]

        if not self.values.__contains__("__parent__"):
            import pdb
            pdb.set_trace()

        if self.values["__parent__"] is None:
            return None

        mro = self.get_mro()
        mro.pop(0)

        for obj in mro:
            if obj.values.__contains__(key):
                return obj.values[key]

        return None

    def istrue(self):
        return True

    def clone(self):
        temp = W_NormalObject(self, str(self.name) + " (clone)")

        for key in self.values:
            if key != "__parent__":
                temp.setvalue(key, self.values[key])

        return temp

    def dump(self, indent = 0):
        dump_result = ("\t"*indent ) + "W_NormalObject (\n\n"

        for key in self.values:
            if not key.startswith("__"):
                if self.values[key] is not None:
                    dump_result += (("\t"*(indent+1)) + str(key) + " = " + self.values[key].dump(indent)) + "\n\n"

        return dump_result + ")"

    def __str__(self):
        if self.getvalue("__parent__") is None:
            parent = "None"
        else:
            parent = str(self.getvalue("__parent__").name)

        return str(self.name) + " (" + parent + ")"


class W_Method(W_NormalObject):
    def __init__(self, reference, parents = None, name = None):
        self.reference = reference
        super(W_Method, self).__init__(parents, name)

    def dump(self, indent = 0):
        dump_result = ("\t"*indent ) + "W_Method()"
        return dump_result

    def clone(self):
        temp = W_Method(self.reference, self, str(self.name) + " (clone)")

        for key in self.values:
            if key != "__parent__":
                temp.setvalue(key, self.values[key])

        return temp


class W_ReturnContextObject(W_NormalObject):
    def __init__(self, parents = None, name = None):
        self.values = {}
        super(W_ReturnContextObject, self).__init__(parents, name)
        self.values["__parent__"] = self.getparents()[-1]


class W_Exception(W_ReturnContextObject):
    def __init__(self, parents = None, name = None, error_number = None):
        self.values = {}
        super(W_Exception, self).__init__(parents, name)

        if error_number is None:
            error_number = W_None()
        elif isinstance(error_number, int):
            error_number = W_Integer(error_number, parents, "int")

        self.values["error_number"] = error_number

    def setParent(self,value):
        self.setvalue("__parent__", value)

    def dump(self, indent = 0):
        dump_result = ("\t"*indent ) + "W_Exception (\n\n"

        for key in self.values:
            if not key.startswith("__"):
                if self.values[key] is not None:
                    dump_result += (("\t"*(indent+1)) + str(key) + " = " + self.values[key].dump(indent)) + "\n\n"

        return dump_result + ")"

class W_BreakObject(W_ReturnContextObject):
    def __init__(self, parents = None, name = None):
        self.values = {}
        super(W_BreakObject, self).__init__(parents, name)


class W_ContinueObject(W_ReturnContextObject):
    def __init__(self, parents = None, name = None):
        self.values = {}
        super(W_ContinueObject, self).__init__(parents, name)


class W_ReturnObject(W_ReturnContextObject):
    def __init__(self, return_value = None, parents = None, name = None):
        self.values = {}
        super(W_ReturnObject, self).__init__(parents, name)
        self.return_value = return_value