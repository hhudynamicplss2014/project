"""
A 'simple' parser.  Don't look into this file :-)
"""
from _csv import Error
import py
import simpleast
from simplelexer import lex
from abc import ABCMeta


def parse(s):
    result = Parser(s).program()
    return result

# ____________________________________________________________

class AbstractParseError(Exception):
    __metaclass__ = ABCMeta

    def __init__(self, source_pos, errorinformation, source=""):
        self.source_pos = source_pos
        self.errorinformation = errorinformation
        self.args = (source_pos, errorinformation)
        self.source = source

    def nice_error_message(self, filename="<unknown>"):
        result = ["  File %s, line %s" % (filename, self.source_pos.lineno + 1)]
        source = self.source
        if source:
            result.append(source.split("\n")[self.source_pos.lineno])
            result.append(" " * self.source_pos.columnno + "^")
        else:
            result.append("<couldn't get source>")
        result.append("ParseError")
        if self.errorinformation:
            failure_reasons = self.errorinformation.expected
            if failure_reasons:
                expected = ''
                if len(failure_reasons) > 1:
                    all_but_one = failure_reasons[:-1]
                    last = failure_reasons[-1]
                    expected = "%s or '%s'" % (
                        ", ".join(["'%s'" % e for e in all_but_one]), last)
                elif len(failure_reasons) == 1:
                    expected = failure_reasons[0]
                if expected:
                    result.append("expected %s" % (expected, ))
            if self.errorinformation.customerror:
                result.append(self.errorinformation.customerror)
        return "\n".join(result)

    def __str__(self):
        return self.nice_error_message()

class SyntacticParseError(AbstractParseError):
    def __init__(self, source_pos, errorinformation, source):
        super(SyntacticParseError, self).__init__(source_pos, errorinformation, source)

class SemanticParseError(AbstractParseError):
    def __init__(self, source_pos, errorinformation, source):
        super(SemanticParseError, self).__init__(source_pos, errorinformation, source)

class ErrorInformation(object):
    def __init__(self, pos, expected=None, customerror=None):
        if expected is None:
            expected = []
        self.expected = expected
        self.pos = pos
        self.customerror = customerror

def combine_errors(self, other):
    if self is None:
        return other
    if (other is None or self.pos > other.pos or
        len(other.expected) == 0):
        return self
    elif other.pos > self.pos or len(self.expected) == 0:
        return other
    failure_reasons = []
    already_there = {}
    for fr in [self.expected, other.expected]:
        for reason in fr:
            if reason not in already_there:
                already_there[reason] = True
                failure_reasons.append(reason)
    return ErrorInformation(self.pos, failure_reasons,
                            self.customerror or other.customerror)

def make_arglist(methodname):
    def arglist(self):
        self.match("OpenBracket", "(")
        method = getattr(self, methodname)
        result = [method()]
        result.extend(self.repeat(self.comma, method))
        self.maybe(self.comma)
        self.match("CloseBracket", ")")
        return result
    return arglist

def make_choice(*methodnames):
    def choice(self):
        c = self.i, self.last_i
        last = None
        for func in methodnames:
            try:
                return getattr(self, func)()
            except SyntacticParseError, e:
                last = combine_errors(last, e.errorinformation)
                self.i, self.last_i = c
        raise SyntacticParseError(self.tokens[self.i].source_pos, last, self.source)
    return choice

class Parser(object):
    def __init__(self, source):
        self.source = source
        self.tokens = lex(source)
        self.i = 0
        self.last_i = 0

    def match(self, name, value=None):
        if self.i < len(self.tokens):
            result = self.tokens[self.i]

            if result.name == name:
                if value is None or result.source == value:
                    self.i += 1
                    return result
        else:
            result = self.tokens[-1]

        raise SyntacticParseError(result.source_pos,
                         ErrorInformation(result.source_pos.i, [value or name]),
                         self.source)

    def call(self, c, *call):
        call = (c, ) + call
        result = None
        for subcall in call:
            result = subcall()
        return result

    def repeat(self, c1, c2=None):
        if c2 is None:
            c2 = c1
            c1 = None
        result = []
        while True:
            c = self.i, self.last_i
            try:
                if c1:
                    c1()
                subresult = c2()
                result.append(subresult)
            except SyntacticParseError, e:
                self.i, self.last_i = c
                return result

    def maybe(self, callable):
        c = self.i, self.last_i
        try:
            return callable()
        except SyntacticParseError, e:
            self.i, self.last_i = c
            return None

    basic_expression = make_choice("intliteral", "implicitselfmethodcall")

    # Expressions
    def expression(self):
        first = self.basic_expression()
        while True:
            following = self.maybe(self.methodcall)
            if not following:
                break
            following.receiver = first
            first = following
        return first

    def intliteral(self):
        token = self.match("Number")
        return simpleast.IntLiteral(int(token.source))

    def name(self):
        return self.match("Name").source

    def newline(self):
        return self.match("Newline")

    def implicitselfmethodcall(self):
        result = self.methodcall()
        result.receiver = simpleast.ImplicitSelf()
        return result

    methodcall = make_choice("normalmethodcall", "primitivemethodcall")

    def normalmethodcall(self):
        name = self.name()
        args = self.maybe(self.callarguments)
        if args is None:
            args = []
        return simpleast.MethodCall(None, name, args)

    def primitivemethodcall(self):
        token = self.match("PrimitiveName")
        args = self.maybe(self.callarguments)
        if args is None:
            args = []
        return simpleast.PrimitiveMethodCall(None, token.source, args)

    
    callarguments = make_arglist("expression")

    def comma(self):
        return self.match('Special', ',')

    # Statements
    def statement(self):
        self.repeat(self.newline)
        return self._statement()

    _statement = make_choice("raisestatement","breakstatement", "continuestatement", "returnstatement", "simplestatement", "ifstatement", "whilestatement",
                             "defstatement", "objectstatement", "beginstatement")

    def statements(self):
        return simpleast.Program(self.repeat(self.statement))

    def program(self):
        result = self.statements()
        self.repeat(self.newline)
        self.match("EOF")
        return result

    def block(self, header, beforeblock=None):
        self.match("Name", header)

        if beforeblock:
            result = self.call(beforeblock)
        else:
            result = None

        self.match("Special", ":")
        self.newline()
        self.match("Indent")
        block = self.statements()
        self.match("Dedent")
        return result, block


    def ifstatement(self):
        (expression, block) = self.block("if", self.expression)
        elseblock = self.maybe(self.elseblock)
        return simpleast.IfStatement(expression, block, elseblock)

    def elseblock(self):
        return self.block("else")[1]

    def rescuestatement(self):
        (ast, rescue) = self.block("rescue", self.rescueheader)
        ast.block = rescue

        return ast
    
    def raisestatement(self):
        result = self.match("Name", "raise")
        name = self.match("Name")
        error_number = self.maybe(self.intliteral)
        self.newline()

        return simpleast.RaiseStatement(name.source, error_number)
    
    def beginstatement(self):
        (expression, block) = self.block("begin")
        (obligatory_rescue_ast, obligatory_rescue_block) = self.block("rescue", self.rescueheader)

        obligatory_rescue_ast.block = obligatory_rescue_block

        rescue_asts = self.repeat(self.rescuestatement)
        rescue_asts.insert(0, obligatory_rescue_ast)

        return simpleast.BeginStatement(block, rescue_asts)

    def rescueheader(self):
        name = self.name()
        return simpleast.RescueStatement(name, None)

    def whilestatement(self):
        (expression, block) = self.block("while", self.expression)
        return simpleast.WhileStatement(expression, block)

    def level_of_indentation(self, start_pos, end_pos):
        if start_pos < end_pos < len(self.tokens):
            level = 0

            for i in range(start_pos, end_pos+1):
                result = self.tokens[i]

                if result.name == 'Indent':
                    level += 1
                elif result.name == 'Dedent':
                    level -= 1

            return level

        return -1

    def position_of_last_command(self, current_position, command):
        if current_position < len(self.tokens):
            for i in range(current_position, -1, -1):
                result = self.tokens[i]

                if result.name == 'Name' and result.source == command:
                    return i

        return -1

    def is_within_indent_of_command(self, current_position, command):
        current_command_position = self.position_of_last_command(current_position, command)

        while current_command_position > 0:
            level_of_indentation = self.level_of_indentation(current_command_position, current_position)
            if level_of_indentation > 0:
                return level_of_indentation
            else:
                current_command_position = self.position_of_last_command(current_command_position-1, command)

        return False

    def is_correct_token(self, current_position, expected_name, expected_source):
        if current_position < len(self.tokens):
            result = self.tokens[current_position]

            return result.name == expected_name and result.source == expected_source

        return False

    def do_indedation_check_on_current_token(self, statement_name, expected_parent):
        if self.is_correct_token(self.i, 'Name', statement_name):
            if not self.is_within_indent_of_command(self.i, expected_parent):
                result = self.tokens[self.i]
                raise SemanticParseError(result.source_pos,
                         ErrorInformation(result.source_pos.i, result.name),
                         self.source)

    def breakstatement(self):
        self.do_indedation_check_on_current_token('break', 'while')

        self.match("Name", "break")
        self.newline()

        return simpleast.BreakStatement()

    def continuestatement(self):
        self.do_indedation_check_on_current_token('continue', 'while')

        self.match("Name", "continue")
        self.newline()

        return simpleast.ContinueStatement()

    def returnstatement(self):
        self.do_indedation_check_on_current_token('return', 'def')

        self.match("Name", "return")
        expr = self.maybe(self.basic_expression)
        self.newline()

        return simpleast.ReturnStatement(expr)


    def defstatement(self):
        (name_args, block) = self.block("def", self.functionheader)
        name = name_args[0]
        args = name_args[1:]
        return simpleast.FunctionDefinition(name, args, block)

    def functionheader(self):
        result = [self.name()]
        args = self.maybe(self._arglist_name)
        if args is not None:
            result.extend(args)
        return result

    _arglist_name = make_arglist("name")
        
    def objectstatement(self):
        (ast, block) = self.block(
            "object", self.objectheader)
        ast.block = block
        return ast

    def objectheader(self):
        name = self.name()
        args = self.maybe(self._arglist_parentdefinition)
        if args is None:
            args = []
        names = [arg.attrname for arg in args]
        expressions = [arg.expression for arg in args]
        return simpleast.ObjectDefinition(name, None, names, expressions)

    _arglist_parentdefinition = make_arglist("parentdefinition")

    def parentdefinition(self):
        name = self.name()
        self.match("Special", "=")
        result = self.expression()
        # XXX hack: use an Assignment node because it fits a bit
        return simpleast.Assignment(None, name, result)

    def simplestatement(self):
        result = self.expression()
        assigntoken = self.tokens[self.i]
        assign = self.maybe(self.assignment)
        self.newline()
        if assign:
            if (isinstance(result, simpleast.MethodCall) and
                    result.arguments == []):
                return simpleast.Assignment(
                        result.receiver, result.methodname, assign)
            else:
                source_pos = assigntoken.source_pos
                raise SyntacticParseError(source_pos,
                                 ErrorInformation(source_pos.i,
                                     customerror="can only assign to attribute"),
                                 self.source)
        return simpleast.ExprStatement(result)

    def assignment(self):
        self.match("Special", "=")
        return self.expression()

