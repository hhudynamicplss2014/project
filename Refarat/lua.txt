- Multiparadigm-language (dynamic, functional, object oriented, ...)
- does have a repl
- created 1993 (inherited from SOL (Simple object language) and DEL (data-entry language))
- other influences:
	Modula (if, while, repeat/until),
	CLU (multi assignments/returns),
	C++ (local variables),
	SNOBOL, AWK (associative arrays), 
	LISP, Scheme
- current version: 5.3 MIT License

- Features:
	namespaces/classes (OO)
	first-class functions (functional programming),
	lexical scoping ensures data safety (hiding variables),
	features can be extended (as needed, like inheritance with metatables),
	ultra light,
	dynamically typed
	garbage collection,
	closures,
	tail calls,
	coercion (automatic conversion betwwen string and numbers at run time),
	dynamic module loading,
	coroutines

- Code:
function factorial(n)
  local x = 1
  for i = 2,n do
    x = x * i
  end
  return x
end


