local prototype = require( "prototype" )

local object = prototype{
  default = prototype.assignment_copy,
}

local bird = object:clone()
bird.name = "bird"
bird.fly = true

function bird:let_my_fly()
	formatted_name = "[" .. self.name .. "]"
	
	if self.fly then 
		print(formatted_name .. " They see my flyin they hatin")
	else
		print(formatted_name .. " If you think you can fly with these wings, you gonna have a bad time")
	end
end	

local eagle = bird:clone()
eagle.name = "Eagle"

local penguin = bird:clone()
penguin.name = "Penguin"		
penguin.fly = false

eagle:let_my_fly()
penguin:let_my_fly()
