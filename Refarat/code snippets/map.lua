function double(arg)
	return 2*arg
end

function add(first, second)
	return first + second
end

local _ = require ("moses")

print("Amount of '1' in {1,1,2,3,3,3,2,4,3,2} is " ..
		_.count( {1,1,2,3,3,3,2,4,3,2}, 1 ) )

print("Double each element of {1,2,3} yields " .. table.concat(
		_.map( {1,2,3}, double ), ","))

print("The sum of {1,2,3,4} is " .. 
		_.reduce( {1,2,3,4}, add ) )

print("Is '3' in {1,2,3,4,5,6,7,8,9}? " .. tostring(
		_.include( {1,2,3,4,5,6,7,8,9}, 3 )) )