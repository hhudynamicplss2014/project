table1 = {0,1,2,3,4,5,6,7}
table2 = {7,6,5,4,3,2,1,0}
table3 = {0}

function my_eq (first, second)
	return first[1] == second[1]
end

function my_add (first, second)
	return first[1] + second[#second]
end

mt1 = { __eq = my_eq ,
		__add = my_add }


setmetatable(table1, mt1)
setmetatable(table2, mt1)
setmetatable(table3, mt1)

print(table1 == table2)
print(table1 == table3)

print(table2 + table1)