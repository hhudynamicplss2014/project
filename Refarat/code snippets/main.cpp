#include <iostream>
using namespace std;

extern "C"
{
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#pragma comment(lib, "lua5.1.lib")

int computeFibonacci(int n)
{
	if(n == 1 || n == 2)
	{
		return 1;
	}

	return computeFibonacci(n-1) + computeFibonacci(n-2);
}

int fibonacci(lua_State* lua)
{
	cout << "Executing C++ function fibonacci" << endl;

	int paramCount = lua_gettop(lua);

	if(paramCount != 1)
	{
		cout << "You didn't provide the correct amount of parameters" << endl;
		return -1;
	}

	int arg = lua_tonumber(lua, paramCount);
	int fib = computeFibonacci(arg);

	lua_pushnumber(lua, fib);
	return 1;
}

int main(int argc, const char* argv[])
{
	lua_State* lua = lua_open();
	luaL_openlibs(lua);

	lua_pushcfunction(lua, fibonacci);
	lua_setglobal(lua, "fibonacci");

	luaL_dofile(lua, "call code.lua");

	lua_close(lua);

	cin.get();
	return 0;
}
