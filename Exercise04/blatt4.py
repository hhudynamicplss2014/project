# Aufgabe 1 - Proxies
def test_logging_proxy_simple():
    class A(object):
        def __init__(self, a):
            self.a = a

        def f(self, x):
            result = self.a
            self.a = x
            return result

    a = A(1)
    p = LoggingProxy(a)
    assert p.a == 1
    attr = p.f(10)
    assert attr == 1
    assert p.a == 10
    assert get_proxy_log(p) == ["a", "f", "a"]

def test_logging_proxy_special():
    p = LoggingProxy(41.0)
    assert p + 2 == 43.0
    assert p - 2 == 39.0
    assert p == 41.0
    assert p
    assert p * 2 == 82.0
    assert p // 2 == 20.0
    assert p > 1.0
    assert get_proxy_log(p) == ["__add__", "__sub__", "__eq__", "__nonzero__",
                                "__mul__", "__floordiv__", "__gt__"]

class HuffmanNode(object):
	def __init__(self, value, frequence, left = None, right = None):
		self.frequence = frequence
		self.value = value
		self.left = left
		self.right = right
	
	def __lt__(self, other):
		return self.value < other.value
		
	def __gt__(self, other):
		return self.value > other.value
		
	def __le__(self, other):
		return self.value <= other.value
		
	def __ge__(self, other):
		return self.value >= other.value
	
	def __str__(self):	
		return "{" + self.value + "," + str(self.frequence) + "," + str(self.left) + "," + str(self.right) + "}"

def insort(array, entry):
	array.append(entry)
	return sorted(array)	
	
def make_tree(mapping):
	sortedMapping = []
	
	for key in mapping:
		node = HuffmanNode(key, mapping[key])
		sortedMapping.append(node)
	
	sortedMapping.sort()
	
	if len(sortedMapping) == 0:
		return None
	else:
		if len(sortedMapping) == 1:
			return sortedMapping[0]		
		
	while len(sortedMapping) > 1:
		first = sortedMapping.pop(0)
		second = sortedMapping.pop(0)
		
		parent = HuffmanNode('*', first.frequence + second.frequence, first, second)		
		sortedMapping = insort(sortedMapping, parent)
	
	return sortedMapping[0]	
	
def make_mapping(tree, mapping = {}, depth = 0, flag = 1):
	if tree.value != "*":
		mapping[tree.value] = ("0" * (depth-1)) + str(flag)
	else:
		make_mapping(tree.left, mapping, depth+1, 0)
		make_mapping(tree.right, mapping, depth+1, 1)
	
	return mapping
	
def encode(mapping, string):
	result = []

	for char in string:
		result.append(mapping[char])
	
	return "".join(result)
		
def getKey(mapping, value):
	for key in mapping:
		if mapping[key] == value:
			return key
			
	return None
	
def parseEncodedString(string, length):
	result = []
	index = 0
	
	while index >= 0:
		next = string.find('1', index)
		if next >= 0:				
			if next - index >= length:
				result.append(string[index:index+length])
				index = index+length				
			else:
				result.append(string[index:next+1])
				index = next+1			
		else:
			index = next
			
	return result
			
def decode(tree, string):
	mapping = make_mapping(tree)
	baseElement = max(mapping, key = lambda s : len(mapping[s]))
	length = len(mapping[baseElement])
	result = []
		
	list = parseEncodedString(string, length)
	
	for i in range(0,len(list)):		
		result.append(getKey(mapping, list[i]))
			
	return "".join(result)
		
# Aufgabe 2 - Huffman Coding
def test_huff_make_coding():
    freq = {"a": 1, "b": 2, "c": 5, "d": 18}
    tree = make_tree(freq)
    assert tree.right.value == "d"
    assert tree.left.right.value == "c"
    assert tree.left.left.right.value == "b"
    assert tree.left.left.left.value == "a"

def test_huff_mapping():
    freq = {"a": 1, "b": 2, "c": 5, "d": 18}
    tree = make_tree(freq)
    mapping = make_mapping(tree)
    assert mapping["a"] == "000"
    assert mapping["b"] == "001"
    assert mapping["c"] == "01"
    assert mapping["d"] == "1"

def test_huff_encode():
    freq = {"a": 1, "b": 2, "c": 5, "d": 18}
    tree = make_tree(freq)
    mapping = make_mapping(tree)
    s = encode(mapping, "ddaccddb")
    assert s == "11000010111001"

def test_huff_decode():
    freq = {"a": 1, "b": 2, "c": 5, "d": 18}
    tree = make_tree(freq)
    s = decode(tree, "1010001010110100101000101")
    assert s == "dcadccdcbcadc"


# Aufgabe 3 - Special Methods
class Burrows(object):
	def __init__(self, string, shift):
		self.shift = shift
		self.string = string[shift:] + string[:shift]			
		
	def __lt__(self, other):
		return self.string < other.string		
		
	def __str__(self):
		return self.string + " (" + str(self.shift) + ")"
		
	def __getitem__(self, index):
		return self.string[index]

def burrows_wheeler_forward(s):
	strings = []
	for i in range(len(s)):
		strings.append(Burrows(s,i))
		
	strings.sort()
	i = [ x.string for x in strings ].index(s)
	
	output = ''.join([s[-1] for s in strings])
	return (output, i)

def burrows_wheeler_backward(text, index):
	array = []
	for i in range(0,len(text)):
		array.append(text[i] + str(-1 if i == index else i))
		
	array.sort(key = lambda s: s[0])
	#print(array)
	
	result = ""	
	while index >= 0:
		result += array[index][0]
		index = int(array[index][1:])
	
	return result

def test_burrows_forward_transformation():
    # see http://de.wikipedia.org/wiki/Burrows-Wheeler-Transformation
    assert burrows_wheeler_forward("TEXTUEL") == ("UTELXTE", 3)

    input = ("TRENTATRE.TRENTINI.ANDARONO.A.TRENTO"
             ".TUTTI.E.TRENTATRE.TROTTERELLANDO")
    expected_output = ("OIIEEAEO..LDTTNN.RRRRRRRTNTTLE"
                       "AAIOEEEENTRDRTTETTTTATNNTTNNAAO....OU.T")
    assert burrows_wheeler_forward(input) == (expected_output, 60)

def test_burrows_compare():
	string = "TEXTUEL"
	first = Burrows(string, 0)
	second = Burrows(string, 2)
	third = Burrows(string, 3)
	
	assert (first < second) == True
	assert (first < third) == True
	assert (third < second) == True
	
def test_burrows_composition():
    f = open('blatt4.py', 'r')
    text = f.read()
    f.close()
    (transformed, index) = burrows_wheeler_forward(text)
    output = burrows_wheeler_backward(transformed, index)
    assert output == text
