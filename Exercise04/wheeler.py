class Burrows(object):
	def __init__(self, string, shift):
		self.shift = shift
		#self.string = string[shift:] + string[:shift]
		self.string = string 
		
	def __lt__(self, other):
		#bide strings durchlaufen und buchstabenweise vergleich vermeided string copy
		#best case: strings unterscheiden sich im ersten zeichen. dann wäre der rest egal!
		str1 = self.string[self.shift:] + self.string[:self.shift]
		str2 = other.string[other.shift:] + other.string[:other.shift]
		return str1 < str2
		
	def __str__(self):
		return self.string + " (" + str(self.shift) + ")"
		
	def __getitem__(self, index):
		return self.string[index]

def burrows_wheeler_forward(s):
	strings = []
	for i in range(len(s)):
		strings.append(Burrows(s,i))
		
	strings.sort()
	i = [ x.string for x in strings ].index(s)
	
	output = ''.join([s[-1] for s in strings])
	return (output, i)

def burrows_wheeler_backward(text, index):
	array = []
	for i in range(0,len(text)):
		array.append(text[i] + str(-1 if i == index else i))
		
	array.sort(key = lambda s: s[0])
	#print(array)
	
	result = ""	
	while index >= 0:
		result += array[index][0]
		index = int(array[index][1:])
	
	return result
	
def test_burrows_forward_transformation():
    # see http://de.wikipedia.org/wiki/Burrows-Wheeler-Transformation
    assert burrows_wheeler_forward("TEXTUEL") == ("UTELXTE", 3)

    input = ("TRENTATRE.TRENTINI.ANDARONO.A.TRENTO"
             ".TUTTI.E.TRENTATRE.TROTTERELLANDO")
    expected_output = ("OIIEEAEO..LDTTNN.RRRRRRRTNTTLE"
                       "AAIOEEEENTRDRTTETTTTATNNTTNNAAO....OU.T")
    assert burrows_wheeler_forward(input) == (expected_output, 60)
	
def test_burrows_compare():
	string = "TEXTUEL"
	first = Burrows(string, 0)
	second = Burrows(string, 2)
	third = Burrows(string, 3)
	
	assert (first < second) == True
	assert (first < third) == True
	assert (third < second) == True
	
def test_burrows_composition():
    f = open('wheeler.py', 'r')
    text = f.read()
    f.close()
    (transformed, index) = burrows_wheeler_forward(text)
    output = burrows_wheeler_backward(transformed, index)
    assert output == text
