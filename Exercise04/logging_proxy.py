# Aufgabe 1 - Proxies
class myproperty(object):
	def __init__(self, getter):
		self.getter = getter
		
	def __get__(self, inst, *args):
		#print("getting")
		return self.getter(inst)

def get_proxy_log(obj):
	return obj.__log__()

class LoggingProxy(object):
	def __init__(self, obj):
		self.obj = obj
		self.log = []
		
		# lookup für special methods fangen bei klasse an und nicht bei instanz
		# keine chance mit getattribute abfangen!
		# im konstruktor schleife alle funktionen in obj.__class__.__dict__ abfangen 
		# und in neue funktionen make_f wrappen, die dann dem dict von Loggingproxy hinzugefügt wird.
				
	# def __getattribute__(self, field):
		# if field in [alle funktionen in LoggingProxy]
			
		# elif field not in ["obj"]
			# log.append(field)
			# return self.obj.__getattribute__(self, field)
		
	
	def __log__(self):
		return self.log		
		
	def __get__(self, instance, owener):
		print("ghjk")
		
	def __getattr__(self, name):
		self.log.append(name)
		return self.obj.__dict__[name]
		
class A(object):
	def __init__(self, a):
		self.a = a

	def f(self, x):
		result = self.a
		self.a = x
		return result
			
a = A(1)		
		
p = LoggingProxy(a)
a.f(20)
get_proxy_log(p)		
		
def test_logging_proxy_simple():
    class A(object):
        def __init__(self, a):
            self.a = a

        def f(self, x):
            result = self.a
            self.a = x
            return result

    a = A(1)
    p = LoggingProxy(a)
    assert p.a == 1
    attr = p.f(10)
    assert attr == 1
    assert p.a == 10
    assert get_proxy_log(p) == ["a", "f", "a"]

def test_logging_proxy_special():
    p = LoggingProxy(41.0)
    assert p + 2 == 43.0
    assert p - 2 == 39.0
    assert p == 41.0
    assert p
    assert p * 2 == 82.0
    assert p // 2 == 20.0
    assert p > 1.0
    assert get_proxy_log(p) == ["__add__", "__sub__", "__eq__", "__nonzero__",
                                "__mul__", "__floordiv__", "__gt__"]

								