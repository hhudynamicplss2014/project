class Vector(object):
    def __init__(self, values):
        self.values = values

    def __len__(self):
        return len(self.values)

    def __getitem__(self, i):
        return self.values[i]

    def __eq__(self, other):
        if self.__class__ != other.__class__:
            return False
        return self.values == other.values 

    def __add__(self, other):
        if len(self) != len(other):
            raise ValueError()
        return Vector([x+y for x,y in zip(self.values, other.values)])

    def __mul__(self, scalar):
        import pdb; pdb.set_trace()
        return Vector([x*scalar for x in self.values])

def test_vector():
    c = Vector([1,2,3])
    c2 = Vector([1,2,3])
    assert len(c) == 3
    assert c[1] == 2
    assert c2[0] == 1
    assert c == c2
    assert c is not c2
    assert 5 != c

def test_vector_addition():
    c = Vector([1,2,3])
    c2 = Vector([1,2,3])
    c3 = c + c2
    assert c3[0] == 2
    assert c3[1] == 4
    assert c3[2] == 6

def test_vector_mul():
    v1 = Vector([1,2,3])
    v2 = v1 * 5
    assert v2.values == [5,10,15]
    v3 = 5 * v1
    assert v3.values == [5,10,15]
