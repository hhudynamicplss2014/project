import random, pygame


class Player(object):
    def __init__(self, x, y, speed = 1):
        self.x = x
        self.y = y
        self.target = (x, y)
        self.speed = speed

    def run(self, screen):
        while True:
            # the logic and drawing code of the player should go here
            self.x += self.get_sign(self.target[0] - self.x) * self.speed
            self.y += self.get_sign(self.target[1] - self.y) * self.speed

            yield pygame.draw.rect(screen, (255,255,255,255), (self.x, self.y, 10, 10))

    def get_sign(self, value):
        if value < 0:
            return -1
        elif value > 0:
            return 1
        else:
            return 0

class Attacker(object):
    def __init__(self, player, x, y):
        self.player = player
        self.x = x
        self.y = y

    def run(self, screen):
        while True:
            # the logic and drawing code of the attacker should go here
            if random.randrange(3) > 0:
                self.x += self.get_sign(random.randint(0,1000)-random.randint(0,1000))
                self.y += self.get_sign(random.randint(0,1000)-random.randint(0,1000))
            else:
                self.x += self.get_sign(self.player.target[0] - self.x)
                self.y += self.get_sign(self.player.target[1] - self.y)

            yield pygame.draw.rect(screen, (0,0,0,255), (self.x, self.y, 10, 10))

    def get_sign(self, value):
        if value < 0:
            return -1
        elif value > 0:
            return 1
        else:
            return 0


title = "My game"

# The framerate of the game (in milliseconds)
framerate = 60

screen_dimensions = (750, 500)

def main():
    # Initialize pygame elements
    screen, clock = init_screen()

    # Build the state
    player = Player(screen_dimensions[0] - 40,
                    screen_dimensions[1] // 2, 2)
    sprites = [player.run(screen)]
    for i in range(10):
        attacker = Attacker(player, 20, random.randrange(screen_dimensions[1]))
        sprites.append(attacker.run(screen))

    # Even loop
    while True:
        # Erase the drawing, by drawing a blank background screen
        screen.fill((192, 144, 0))

        # Ask each sprite to move one step and draw itself
        for sprmover in sprites:
            next(sprmover)

        # Update the screen
        pygame.display.flip()
        clock.tick(framerate)

        # Look out for window close events:
        for e in pygame.event.get():
            if e.type == pygame.MOUSEMOTION:
                player.target = e.pos
            if e.type == pygame.QUIT:
                return


def init_screen():
    pygame.init()
    screen = pygame.display.set_mode(screen_dimensions)
    pygame.display.set_caption(title)
    clock = pygame.time.Clock()
    return screen, clock

main()
