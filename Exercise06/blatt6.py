import itertools


# ========== AUFGABE 1 ==========

class TreeNode(object):

    def __init__(self, value, left, right):
        self.value = value
        self.left = left
        self.right = right
        self.current = 0

    def enumerate(self):
        pass   # write a generator here...

    def enumerate_first(self):
        #a = self.value
#
#        while True:
#            yield a
#            a = self.enumerate_first()
        result = str(self.value)

        if self.left is not None:
            result += "," + self.left.enumerate_first()

        if self.right is not None:
            result += "," + self.right.enumerate_first()

        return result

    def enumerate_first_gen(self):
        value = self.value
        self.current = 0

        while self.current < 2:
            print(value)
            yield value

            if self.has_children():
                if self.current == 0:
                    if self.left is not None:
                        iterable = self.left.enumerate_first_gen()
                        for x in iterable:
                            value = next(iterable)

                    self.current = 1
                elif self.current == 1:
                    if self.right is not None:
                        iterable = self.right.enumerate_first_gen()

                        for x in iterable:
                            value = next(iterable)

                    self.current = 2
            else:
                value = StopIteration

    def enumerate_last(self):
        pass   # write a generator here...

    def has_children(self):
        return self.left is not None or self.right is not None


tree1 = TreeNode(5, TreeNode(3, TreeNode(1, None,
                                                TreeNode(2, None,
                                                            None)),
                                    TreeNode(4, None,
                                                None)),
                        TreeNode(7, TreeNode(6, None,
                                                None),
                                    TreeNode(8, None,
                                                TreeNode(9, None,
                                                            None))))

g = tree1.enumerate_first_gen()

def test_tree1():
    tree1 = TreeNode(5, TreeNode(3, TreeNode(1, None,
                                                TreeNode(2, None,
                                                            None)),
                                    TreeNode(4, None,
                                                None)),
                        TreeNode(7, TreeNode(6, None,
                                                None),
                                    TreeNode(8, None,
                                                TreeNode(9, None,
                                                            None))))
    assert list(tree1.enumerate()) == [1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert list(tree1.enumerate_first()) == [5, 3, 1, 2, 4, 7, 6, 8, 9]
    assert list(tree1.enumerate_last()) == [2, 1, 4, 3, 6, 9, 8, 7, 5]

def sum(iterable):
    result = 0

    while True:
        yield result
        result += next(iterable)

def test_sum_iterable():
    s_int_iterable = sum(itertools.count())
    summe = 0

    for i in range(1000000):
        assert next(s_int_iterable) == summe
        summe += i